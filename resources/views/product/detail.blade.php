@extends('layouts.master')
@section('css')
<link href="{{asset('frontend/asset/css/evo-products.scss.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('title') {{$product->name}} @endsection
@section('description') {{$product->description}} @endsection 
@section('url'){{url('/san-pham/'.$product->slug)}}@endsection
@section('image')
   @php
      $image =  json_decode($product->images) ;
   @endphp
{{$image[0]}} 
@endsection

@section('content')
<section class="bread-crumb margin-bottom-10">
        <div class="container">
           <div class="row">
              <div class="col-xs-12">
                 <ul class="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <li class="home"><a itemprop="url" href="/" title="Trang chủ"><span itemprop="title">Trang chủ</span></a><span><i class="fa fa-angle-right"></i></span></li>
                    <li><a itemprop="url" href="/san-pham-moi" title="Sản phẩm mới"><span itemprop="title">Sản phẩm mới</span></a><span><i class="fa fa-angle-right"></i></span></li>
                    <li ><strong><span itemprop="title">{{$product->name}}</span></strong>
                    <li>
                 </ul>
              </div>
           </div>
        </div>
     </section>
     <section class="product margin-top-30" itemscope itemtype="http://schema.org/Product">
        <div class="container">
           <div class="row details-product padding-bottom-10">
              <div class="col-md-9 col-lg-9 col-xs-12 col-sm-12 product-bottom">
                 <div class="row">
                    <div class="col-xs-12 col-sm-6 col-lg-6 col-md-6">
                       <div class="relative product-image-block">
                          <div class="slider-big-video clearfix margin-bottom-10">
                             <div class="slider slider-for">
                                @php
                                    $image =  json_decode($product->images) ;
                                @endphp
                                @foreach ($image as $item)
                                    <a href="{{$item}}" title="Click để xem">
                                        <img src="{{$item}}" class="img-responsive center-block">
                                    </a>
                                @endforeach
                             </div>
                          </div>
                          <div class="slider-has-video clearfix">
                             <div class="slider slider-nav">
                                @foreach ($image as $item)
                                <div class="fixs">
                                   <img class="lazy" src="{{$item}}" data-image="{{$item}}" />
                                </div>
                                @endforeach
                             </div>
                          </div>
                       </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-lg-6 col-md-6 details-pro">
                       <div class="product-top clearfix">
                          <h1 class="title-head">{{$product->name}}</h1>
                          <div class="sku-product clearfix">
                             <div class="item-sku">
                                SKU: <span class="variant-sku" itemprop="sku" content="Đang cập nhật">{{$product->code}}</span>
                                <span class="hidden" itemprop="brand" itemscope itemtype="https://schema.org/brand">RIBETO</span>
                             </div>
                             {{-- <div class="item-sku">
                                Thương hiệu:
                                <span class="vendor">
                                Cocoxim
                                </span>
                             </div> --}}
                          </div>
                       </div>
                       <div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
                        @if($product->discount)
                            <div class="price-box clearfix">
								<span class="special-price">
									<span class="price product-price">{{number_format($product->price-($product->price*($product->discount/100)))}}₫</span>
								</span> <!-- Giá Khuyến mại -->
								<span class="old-price" itemprop="priceSpecification" itemscope="" itemtype="http://schema.org/priceSpecification">
									Giá thị trường:
									<del class="price product-price-old">
                      {{number_format($product->price)}}đ
									</del>
								</span> <!-- Giás gốc -->
								<span class="save-price">Tiết kiệm:
									<span class="price product-price-save">{{$product->price*($product->discount/100)}}₫</span>
								</span> <!-- Tiết kiệm -->
                            </div>
                        @else 
                            <div class="price-box clearfix">
              								<span class="special-price">
                                 @if($product->price == 0)
                                    <p>Giá sản phẩm: Liên hệ</p>
                                  @else
                                    <span class="price product-price">Giá sản phẩm: {{number_format($product->price)}}₫</span>
                                  @endif
              									
              								</span>
                            </div>
                        @endif
                          <div class="inventory_quantity">
                             <span class="stock-brand-title">Tình trạng:</span>
                             <span class="a-stock a2">
                                <link itemprop="availability" href="http://schema.org/InStock" />
                                Còn hàng
                             </span>
                          </div>
                          <div class="evo-product-summary product_description margin-bottom-10">
                             <a class="evo-product-tabs-header hidden-lg hidden-md hidden-sm" href="javascript:void(0);">
                                <span>Tổng quan</span>
                                <svg xmlns="http://www.w3.org/2000/svg" width="5.658" height="9.903" viewBox="0 0 5.658 9.903">
                                   <path d="M5429 1331.94l4.451 4.451-4.451 4.452" stroke="#1c1c1c" stroke-linecap="round" fill="none" transform="translate(-5428.5 -1331.44)"></path>
                                </svg>
                             </a>
                             <div class="rte description rte-summary">
                                <ul>
                                   <li>Bao bì sản phẩm có thể thay đổi theo Nhà cung cấp</li>
                                   <li>Sản xuất trên quy trình công nghệ hiện đại</li>
                                   <li>Đảm bảo an toàn cho người tiêu dùng</li>
                                   <li>Được đóng hộp tiện lợi cho sử dụng và bảo quản</li>
                                </ul>
                             </div>
                          </div>
                       </div>
                       <div class="form-product">
                          <form enctype="multipart/form-data" id="add-to-cart-form" action="/cart/add" method="post" class="clearfix form-inline">
                             <div class="box-variant clearfix  hidden ">
                                <input type="hidden" name="variantId" value="25835031" />
                             </div>
                             <div class="clearfix form-group ">
                                <div class="qty-ant clearfix custom-btn-number">
                                   <label>Số lượng:</label>
                                   <div class="custom custom-btn-numbers form-control">		
                                      <button onclick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN(qty) & qty > 1 ) result.value--;return false;" class="btn-minus btn-cts" type="button">–</button>
                                      <input type="text" class="qty input-text" id="qty" name="quantity" size="4" value="1" maxlength="3" />
                                      <button onclick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN(qty)) result.value++;return false;" class="btn-plus btn-cts" type="button">+</button>
                                   </div>
                                </div>
                                <div class="btn-mua">
                                    <a href="{{route('cart.now',['id'=>$product->id])}}" class="btn btn-lg btn-gray">
                                      <span class="txt-main">Mua ngay</span><br>
                                      <span class="text-add">Đặt mua giao hàng tận nơi</span>
                                   </a>
                                </div>
                             </div>
                          </form>
                       </div>
                    </div>
                 </div>
              </div>
              <div class="col-md-3 col-lg-3 col-xs-12 col-sm-12 hidden-sm hidden-xs">
                 <div class="ant_onlineSupport">
                    <h2 class="supportTitle">CHÚNG TÔI LUÔN SẴN SÀNG<br>ĐỂ GIÚP ĐỠ BẠN</h2>
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-src="https://bizweb.dktcdn.net/100/360/151/themes/727143/assets/ant_product_support.png" alt="Hỗ trợ trực tuyến" class="lazy supportImage img-responsive center-block" />
                    <h3 class="supportTitle3">Để được hỗ trợ tốt nhất. Hãy gọi</h3>
                    <div class="phoneNumber">
                       <a href="tel:{{$setting->phone1}}" title="{{$setting->phone1}}">{{$setting->phone1}}</a>
                    </div>
                    <div class="or"><span>HOẶC</span></div>
                    <h3 class="supportTitle">Để được hỗ trợ tốt nhất</h3>
                    <a class="chatNow sprite-icon_chat" target="_blank" rel="nofollow" href="https://www.facebook.com" title="CHAT VỚI CHÚNG TÔI">CHAT VỚI CHÚNG TÔI</a>
                 </div>
              </div>
              <div class="col-xs-12 col-sm-8 col-lg-9 col-md-9 margin-top-20">
                 <div class="product-tab e-tabs padding-bottom-10 evo-tab-product-mobile">
                    <ul class="tabs tabs-title clearfix hidden-xs">
                       <li class="tab-link" data-tab="tab-1">Mô tả</li>
                       <li class="tab-link" data-tab="tab-2">Đánh giá</li>
                    </ul>
                    <div id="tab-1" class="tab-content active">
                       {!!$product->content!!}
                    </div>
                    <div id="tab-2" class="tab-content">
                       <div class="col-sm-12">
                              <div class="fb-comments" data-href="{{route('blog.detail',['slug'=>$product->slug])}}" data-width="100%" data-numposts="5"></div>
                           </div>
                    </div>
                 </div>
              </div>
              <div class="col-xs-12 col-sm-4 col-lg-3 col-md-3 margin-top-20">
                 <div class="aside-item evo-product-article">
                    <h2 class="title-head">
                       <a href="tin-tuc" title="Tin khuyến mãi">Tin cập nhật</a>
                    </h2>
                    <div class="list-blogs">
                        @foreach($blog as $item)
                            <article class="blog-item blog-item-list clearfix">
                            <a href="/bach-tuoc-nuong-va-5-cach-che-bien-thom-ngon-de-ban-don-mua-mua-ve" title="Bạch tuộc nướng v&#224; 5 c&#225;ch chế biến thơm ngon để bạn đ&#243;n m&#249;a mưa về">
                                <div class="panel-box-media">
                                <img class="lazy" src="{{$item->image}}" data-src="{{$item->image}}" alt="{{$item->title}}" />
                                </div>
                                <div class="blogs-rights">
                                    <h3 class="blog-item-name">
                                        {{$item->title}}
                                    </h3>
                                </div>
                            </a>
                            </article>
                        @endforeach
                    </div>
                    <div class="blogs-mores text-center"><a href="{{ route('blog.list') }}" title="Xem thêm">Xem thêm</a></div>
                 </div>
              </div>
           </div>
           <div class="row margin-top-20 margin-bottom-10">
              <div class="col-lg-12">
                 <div class="related-product">
                    <div class="home-title text-center">
                       <h2><a href="/san-pham-moi" title="Sản phẩm liên quan">Sản phẩm liên quan</a></h2>
                    </div>
                    <div class="evo-owl-product clearfix">
                        @foreach ($product_relate as $item)
                        <div class="evo-slick">
                                <div class="evo-product-block-item ">
                                   <div class="product-img">
                                   <a href="{{ route('product.detail', ['slug' => $item->slug]) }}" title="{{$item->name}}" class="image-resize">
                                      <img class="lazy" src="{{json_decode( $item->images)[0]}}" data-src="{{json_decode($item->images)[0]}}" alt="{{$item->name}}" />
                                      </a>
                                      <div class="button-add">
                                      <div 
                                            class="hidden-md variants form-nut-grid"
                                            >
                                            <button type="button" title="Mua ngay">
                                               <a style="color:#fff" href="{{route('cart.now',['id'=>$item->id])}}"
                                               title="Mua ngay">Mua ngay</a>
                                            </button>
                                            <button class=""
                                               data-toggle="modal" data-target="#popup-cart" data-id="{{$item->id}}">Thêm vào
                                               giỏ</button>
                                      </div>
                                </div>
                                   </div>
                                   <div class="product-detail clearfix">
                                      <div class="box-pro-detail">
                                         <h3 class="pro-name">
                                            <a href="{{ route('product.detail', ['slug' => $item->slug]) }}" title="{{$item->name}}">{{$item->name}}</a>
                                         </h3>

                                         <div class="box-pro-prices">
                                            <p class="pro-price">
                                               @if($product->price == 0)
                                                Liên hệ
                                              @else
                                                {{number_format($product->price)}}₫
                                              @endif
                                            </p>
                                         </div>
                                      </div>
                                   </div>
                                </div>
                             </div>
                        @endforeach
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </section>
     <script>
        $(document).on('click', '.btn-buy-now-click', function(e) {
            e.preventDefault();
            $('[data-role=addtocart]').click();
        });
        $('.evo-owl-product').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 5,
            slidesToScroll: 5,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 5
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        dots: true
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        dots: true
                    }
                }
            ]
        });
        $('.slider-nav').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            dots: false,
            centerMode: false,
            infinite: false,
            focusOnSelect: true,
            responsive: [
                {
                    breakpoint: 1025,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 4
                    }
                }
            ]
        });
        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            lazyLoad: 'ondemand',
            fade: true,
            infinite: false,
            asNavFor: '.slider-nav',
            adaptiveHeight: false
        });
        $('.slider-big-video .slider-for a').each(function() {
            $(this).attr('rel','lightbox-demo'); 
        });
     </script>
@endsection
@section('script_detail')
<script>
        $(document).on('click', '.btn-buy-now-click', function(e) {
            e.preventDefault();
            $('[data-role=addtocart]').click();
        });
        $('.evo-owl-product').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 5,
            slidesToScroll: 5,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 5
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        dots: true
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        dots: true
                    }
                }
            ]
        });
        $('.slider-nav').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            dots: false,
            centerMode: false,
            infinite: false,
            focusOnSelect: true,
            responsive: [
                {
                    breakpoint: 1025,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 4
                    }
                }
            ]
        });
        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            lazyLoad: 'ondemand',
            fade: true,
            infinite: false,
            asNavFor: '.slider-nav',
            adaptiveHeight: false
        });
        $('.slider-big-video .slider-for a').each(function() {
            $(this).attr('rel','lightbox-demo'); 
        });
     </script> 
@endsection