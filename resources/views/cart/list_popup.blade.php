<div class="tbody-popup" >
        @foreach ($cart as $item)
        <div class="item-popup itemid-25835028">
            <div style="width: 55%;" class="text-left">
                <div class="item-image">
                <a class="item-image" href="{{ route('product.detail', ['slug' => $item->options->slug]) }}" title="{{$item->name}}"><img alt="{{$item->name}}" src="{{$item->options->img ? $item->options->img : '' }}" width="80"></a>
                </div>
                <div class="item-info">
                    <p class="item-name"><a href="{{ route('product.detail', ['slug' => $item->options->slug]) }}" title="{{$item->name}}">{{$item->name}}</a></p>
                    <p class="variant-title-popup"></p>
                    <p class="item-remove"><a href="javascript:;" onclick="deleteCart('{{$item->rowId}}')" title="Xóa"><i class="fa fa-close"></i> Bỏ sản phẩm</a></p>
                </div>
            </div>
            <div style="width: 15%;" class="text-right">
                @if($item->discount)
                    <div class="item-price"><span class="price pricechange">{{number_format($item->price-($item->price*($item->discount/100)))}}₫</span></div>
                @else 
                    <div class="item-price"><span class="price pricechange">{{number_format($item->price)}}₫</span></div>
                @endif
            </div>
            <div style="width: 15%;" class="text-center">
                <button onclick="downQty('qtyItem{{$item->options->code}}','{{$item->rowId}}',{{$item->id}})" class="reduced items-counts btn-minus" type="button">–</button>
                <input disabled="" type="text" maxlength="12" min="0"  id="qtyItem{{$item->options->code}}" name="Lines" class="input-text number-sidebar" size="4" value="{{$item->qty}}">
                <button onclick="upQty('qtyItem{{$item->options->code}}','{{$item->rowId}}',{{$item->id}})" class="increase items-counts btn-plus" type="button">+</button>
            </div>
            <div style="width: 15%;" class="text-right"><span class="cart-price"> <span class="price">{{number_format($item->price * $item->qty)}}₫</span> </span>
            </div>
        </div>
    @endforeach
</div>
<div class="tfoot-popup">
    <div class="tfoot-popup-1 clearfix">
        <div class="pull-left popupcon"><a class="button btn-continue" title="Tiếp tục mua hàng"
                onclick="$('#popup-cart').modal('hide');"><span><span><i class="fa fa-caret-left"
                            aria-hidden="true"></i> Tiếp tục mua hàng</span></span></a></div>
        <div class="pull-right popup-total">
        <p>Thành tiền: {{$total}}đ<span class="total-price"></span></p>
        </div>
    </div>
    <div class="tfoot-popup-2 clearfix"><a class="button btn-proceed-checkout"
            title="Thanh toán đơn hàng" href="{{route('pay.index')}}"><span>Thanh toán đơn hàng</span></a></div>
</div>
