<?php

namespace App\models\product;

use Illuminate\Database\Eloquent\Model;
use App\models\language\Language;

class Product extends Model
{
    protected $table = "products";
    protected $fillable = array(
        'language',
        'name',
        'slug',
        'code',
        'price',
        'images',
        'qty',
        'description',
        'content',
        'category',
        'status'
    );
    public function rule()
    {
        return [
            // 'name'  =>   'unique:products,name'
        ];
    }
    public function createOrEdit($request)
    {
        $lang = $request->language;
        $code = $request->code;
        if($lang != "" && $code != ""){
            $query = Product::where([
                'code' => $code,
                'language'=> $lang
             ])->first();
            if($query){
                $query->name = $request->name;
                $query->price = $request->price;
                $query->images = json_encode($request->images);
                $query->qty = $request->qty;
                $query->description = $request->description;
                $query->content = $request->content;
                $query->category = $request->category;
                $query->discount = $request->discount;
                $query->status = $request->status;
                $query->save();
            }else{
                $query = new Product();
                $query->name = $request->name;
                $query->slug = to_slug($request->name);
                $query->code =  $request->code;
                $query->price = $request->price;
                $query->images = json_encode($request->images);
                $query->qty = $request->qty;
                $query->language = $request->language;
                $query->description = $request->description;
                $query->content = $request->content;
                $query->category = $request->category;
                $query->discount = $request->discount;
                $query->status = $request->status;
                $query->save();
            }
            $query = Product::where([
                'code' => $code,
                'language'=> $lang
             ])->update(
             [
                'name' => $request->name,
                'price' => $request->price,
                'images' => json_encode($request->images),
                'qty' => $request->qty,
                'description' => $request->description,
                'content' => $request->content,
                'category' => $request->category,
                'discount' => $request->discount,
                'status' => $request->status
            ]);
            return $query;
        }else{
            $listLanguage = Language::get();
            $msp = rand();
            foreach ($listLanguage as $item) {
                $query = new Product();
                $query->name = $request->name;
                $query->slug = to_slug($request->name);
                $query->code = $msp;
                $query->price = $request->price != null ? $request->price : 0;
                $query->images = json_encode($request->images);
                $query->qty = $request->qty;
                $query->language = $item['code'];
                $query->description = $request->description;
                $query->content = $request->content;
                $query->category = $request->category;
                $query->discount = $request->discount;
                $query->status = $request->status;
                $query->save();
            }
            return $query;
        }
    }
}
