@extends('layouts.master')
@section('css')
<link href="{{asset('frontend/asset/css/evo-article.scss.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('frontend/asset/css/evo-collections.scss.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('title') {{$blog_detail->title}} @endsection
@section('description') {{$blog_detail->description}} @endsection 
@section('url') {{route('blog.detail',['slug'=>$blog_detail->slug])}} @endsection
@section('image') {{$blog_detail->image}} @endsection
@section('content')
<section class="bread-crumb margin-bottom-10">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">					
					<li class="home"><a itemprop="url" href="/" title="Trang chủ"><span itemprop="title">Trang chủ</span></a><span><i class="fa fa-angle-right"></i></span></li>
					<li><a itemprop="url" href="/tin-tuc" title="Tin tức"><span itemprop="title">Tin tức</span></a><span><i class="fa fa-angle-right"></i></span></li>
					<li><strong itemprop="title">{{$blog_detail->title}}</strong></li>
				</ul>
			</div>
		</div>
	</div>
</section>
<div class="container article-wraper margin-top-20">
   <div class="row">
      <section class="right-content col-md-9 col-md-push-3">
         <article class="article-main" itemscope="" itemtype="http://schema.org/Article">
            <div class="row">
               <div class="col-md-12 evo-article margin-bottom-10">
                  <h1 class="title-head">{{$blog_detail->title}}</h1>
                  <div class="article-summary">
                  </div>
                  <div class="postby">
                     <svg viewBox="0 0 101 95" data-radium="true" style="width: 16px; height: 16px;">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                           <g transform="translate(-570.000000, -1905.000000)" fill="rgba(0, 0, 0, .6)">
                              <path d="M620.585796,1958.31466 C635.364122,1958.31466 647.350985,1946.35776 647.350985,1931.61638 C647.350985,1916.875 635.364122,1905 620.585796,1905 C605.807471,1905 593.820608,1916.9569 593.820608,1931.61638 C593.820608,1946.27586 605.807471,1958.31466 620.585796,1958.31466 L620.585796,1958.31466 Z M620.585796,1911.96121 C631.423235,1911.96121 640.29023,1920.80603 640.29023,1931.61638 C640.29023,1942.42672 631.423235,1951.27155 620.585796,1951.27155 C609.748358,1951.27155 600.881363,1942.42672 600.881363,1931.61638 C600.881363,1920.80603 609.748358,1911.96121 620.585796,1911.96121 Z M574.280378,2000 L574.280378,2000 C572.309934,2000 570.75,1998.44397 570.75,1996.47845 C570.75,1977.96983 585.856732,1962.98276 604.329639,1962.98276 L637.170361,1962.98276 C655.725369,1962.98276 670.75,1978.05172 670.75,1996.47845 C670.75,1998.44397 669.190066,2000 667.219622,2000 L574.280378,2000 Z M604.329639,1970.02586 C590.864943,1970.02586 579.781199,1980.09914 578.057061,1992.9569 L663.442939,1992.9569 C661.718801,1980.01724 650.635057,1970.02586 637.170361,1970.02586 L604.329639,1970.02586 Z" id="profile"></path>
                           </g>
                        </g>
                     </svg>
                     Tác giả: <b>{{$blog_detail->author}}</b> 
                     <svg viewBox="0 0 101 100" data-radium="true" style="width: 16px; height: 16px;">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                           <g transform="translate(-295.000000, -1086.000000)" fill="rgba(0, 0, 0, .6)">
                              <path d="M395.5,1136 C395.5,1108.36615 373.047496,1086 345.5,1086 C317.952504,1086 295.5,1108.36615 295.5,1136 C295.5,1163.5475 317.866149,1186 345.5,1186 C373.133851,1186 395.5,1163.5475 395.5,1136 Z M388.505181,1136 C388.505181,1159.74784 369.247841,1179.00518 345.5,1179.00518 C321.752159,1179.00518 302.494819,1159.66149 302.494819,1136 C302.494819,1112.25216 321.752159,1092.99482 345.5,1092.99482 C369.247841,1092.99482 388.505181,1112.25216 388.505181,1136 Z M359.83506,1141.87219 C361.821244,1141.87219 363.375648,1140.31779 363.375648,1138.33161 C363.375648,1136.34542 361.821244,1134.79102 359.83506,1134.79102 L346.708981,1134.79102 L346.708981,1114.23834 C346.708981,1112.25216 345.154577,1110.69775 343.168394,1110.69775 C341.182211,1110.69775 339.627807,1112.25216 339.627807,1114.23834 L339.627807,1138.33161 C339.627807,1140.31779 341.182211,1141.87219 343.168394,1141.87219 L359.83506,1141.87219 Z"></path>
                           </g>
                        </g>
                     </svg>
                     Ngày đăng: {{date_format($blog_detail->created_at,'d/m/Y')}}
                  </div>
                  <div class="article-details">
                  	{!! $blog_detail->content !!}
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-12">
                  <div class="evo-article-toolbar">
                     <div class="evo-article-toolbar-left clearfix">
                        <span class="evo-article-toolbar-head">Bạn đang xem: </span>
                        <span class="evo-article-toolbar-title" title="{{$blog_detail->title}}">{{$blog_detail->title}}</span>
                     </div>
                     <div class="evo-article-toolbar-right">
                        <a href="javascript:;" title="Bài trước">
                           <svg class="Icon Icon--select-arrow-left" role="presentation" viewBox="0 0 11 18">
                              <path d="M9.5 1.5L1.5 9l8 7.5" stroke-width="2" stroke="currentColor" fill="none" fill-rule="evenodd" stroke-linecap="square"></path>
                           </svg>
                           Bài trước
                        </a>
                     </div>
                  </div>
               </div>
               <div class="col-md-12 margin-top-10">
                  <form accept-charset="UTF-8" action="/posts/bach-tuoc-nuong-va-5-cach-che-bien-thom-ngon-de-ban-don-mua-mua-ve/comments" id="article_comments" method="post" class="has-validation-callback">
                     <input name="FormType" type="hidden" value="article_comments">
                     <input name="utf8" type="hidden" value="true"> 
                     <div class="form-coment margin-bottom-10 margin-top-20">
                        <h5 class="title-form-coment">VIẾT BÌNH LUẬN CỦA BẠN</h5>
                        <div class="row">
                           <div class="col-sm-12">
                              <div class="fb-comments" data-href="{{route('blog.detail',['slug'=>$blog_detail->slug])}}" data-width="100%" data-numposts="5"></div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </article>
      </section>
      @include('layouts.sidebar')
   </div>
</div>
@endsection