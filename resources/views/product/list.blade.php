@extends('layouts.master')
@section('css')
<link href="{{asset('frontend/asset/css/evo-collections.scss.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('title') Danh sách sản phẩm của Ribeto @endsection
@section('description') {{$setting->fax}} @endsection 
@section('url') {{ url('/san-pham') }} @endsection
@section('image') {{ $setting->logo }} @endsection

@section('content')
<section class="bread-crumb margin-bottom-10">
   <div class="container">
      <div class="row">
         <div class="col-xs-12">
            <ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
               <li class="home"><a itemprop="url" href="/" title="Trang chủ"><span itemprop="title">Trang chủ</span></a><span><i class="fa fa-angle-right"></i></span></li>
               <li><strong><span itemprop="title">Tất cả sản phẩm</span></strong></li>
            </ul>
         </div>
      </div>
   </div>
</section>
<div class="container margin-top-20">
   <div class="row">
      <section class="main_container collection col-md-9 col-md-push-3">
         <h1 class="col-title">Tất cả sản phẩm</h1>
         <div class="category-gallery">
            <div class="single_image_effect">
               <img src="//bizweb.dktcdn.net/100/360/151/themes/727143/assets/cat-banner-1.jpg?1564021130230" data-src="//bizweb.dktcdn.net/100/360/151/themes/727143/assets/cat-banner-1.jpg?1564021130230" alt="Tất cả sản phẩm" title="Tất cả sản phẩm" class="lazy img-responsive center-block loaded" data-was-processed="true">
            </div>
         </div>
         <div class="category-products products category-products-grids">
            <div class="sort-cate clearfix margin-bottom-10 hidden-xs">
               <div class="sort-cate-left hidden-xs">
                  <h3>Xếp theo:</h3>
                  <ul>
                     <li class="btn-quick-sort alpha-asc">
                        <a href="javascript:;" onclick="sortby('alpha-asc')" title="Tên A-Z"><i></i>Tên A-Z</a>
                     </li>
                     <li class="btn-quick-sort alpha-desc">
                        <a href="javascript:;" onclick="sortby('alpha-desc')" title="Tên Z-A"><i></i>Tên Z-A</a>
                     </li>
                     <li class="btn-quick-sort position-desc">
                        <a href="javascript:;" onclick="sortby('created-desc')" title="Hàng mới"><i></i>Hàng mới</a>
                     </li>
                     <li class="btn-quick-sort price-asc">
                        <a href="javascript:;" onclick="sortby('price-asc')" selected title="Giá thấp đến cao"><i></i>Giá thấp đến cao</a>
                     </li>
                     <li class="btn-quick-sort price-desc">
                        <a href="javascript:;" onclick="sortby('price-desc')" title="Giá cao xuống thấp"><i></i>Giá cao xuống thấp</a>
                     </li>
                  </ul>
               </div>
            </div>
            <section class="products-view products-view-grid row" id="productData">
                @if(count($data) == 0)
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="evo-product-block-item ">
                            <p class="pro-price">
                                    Chưa có sản phẩm phù hợp
                            </p>
                    </div>
                </div>
                @else
                    @foreach ($data as $item)
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                            <div class="evo-product-block-item ">
                            <div class="product-img">
                                @if($item['discount'])
                                    <div class="product-sale" >
                                    <span>{{$item['discount']}}%</span>
                                    </div>
                                @endif
                                <a href="{{ route('product.detail', ['slug' => $item->slug]) }}" title="{{$item->name}}" class="image-resize">
                                <img class="lazy loaded" src="{{json_decode($item->images)[0]}}" data-src="{{json_decode($item->images)[0]}}" alt="{{json_decode($item->images)[0]}}" data-was-processed="true">
                                </a>
                                <div class="button-add">
                                    <div 
                                          class="hidden-md variants form-nut-grid"
                                          >
                                          <button type="button" title="Mua ngay">
                                             <a style="color:#fff" href="{{route('cart.now',['id'=>$item->id])}}"
                                             title="Mua ngay">Mua ngay</a>
                                          </button>
                                          <button class=""
                                             data-toggle="modal" data-target="#popup-cart" data-id="{{$item->id}}">Thêm vào
                                             giỏ</button>
                                    </div>
                                   
                                </div>
                            </div>
                            <div class="product-detail clearfix">
                                <div class="box-pro-detail">
                                    <h3 class="pro-name">
                                    <a href="{{ route('product.detail', ['slug' => $item->slug]) }}" title="{{$item->name}}">{{$item->name}}</a>
                                    </h3>
                                    <div class="box-pro-prices">
                                        @if($item['discount'])
                                            <p class="pro-price">
                                            {{number_format($item['price']-($item['price']*($item['discount']/100)))}}₫
                                                <span class="pro-price-del">
                                                    <del class="compare-price">{{number_format($item['price'])}}</del>
                                                </span>
                                            </p>
                                        @else
                                            <p class="pro-price">
                                                {{number_format($item['price'])}}₫
                                            </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    @endforeach
                @endif
               <div class="clearfix"></div>
               <div class="text-xs-right">
                  <nav class="text-center">
                        {{ $data->links() }}
                  </nav>
               </div>
            </section>
         </div>
      </section>
      @include('layouts.sidebar')
      <div id="open-filters" class="open-filters hidden-lg hidden-md">
         <i class="fa fa-filter" aria-hidden="true"></i>
      </div>
   </div>
</div>
@endsection
@section('script')
    <script>
        function sortby(evt){
            let type_data = evt
            $.ajax({
                type: 'get',
                dataType: 'html',
                url: '{{url("/san-pham/filter-product")}}',
                data: {type:type_data,"_token": "{{ csrf_token() }}"},
                success:function(response){
                    $("#productData").html(response);
                }
                });
        }
    </script>
@endsection