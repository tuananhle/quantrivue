/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.$ = require('jquery')
window.JQuery = require('jquery')
require('../assets/js/misc.js');
import Vue from 'vue';
import axios from 'axios'
import store from './store/index';
import router from './router';
import App from "./components/App.vue"

import Vuelidate from 'vuelidate'
import ImageUpload from './components/layouts/upload_image'
import ToggleButton from 'vue-js-toggle-button'

import Vuesax from 'vuesax';
import 'vuesax/dist/vuesax.css';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'material-icons/iconfont/material-icons.css';
import '@mdi/font/css/materialdesignicons.css';
import Notify from  '../../resources/js/mixin/notify';
import i18n from 'vue-i18n';
import locale from 'element-ui/lib/locale/lang/vi'

import VueChartJs from 'vue-chartjs'
import VueFusionCharts from 'vue-fusioncharts';
import FusionCharts from 'fusioncharts';
import Charts from 'fusioncharts/fusioncharts.charts';

//import the theme
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion'

// register VueFusionCharts component
Vue.use(VueFusionCharts, FusionCharts, Charts, FusionTheme)
Vue.use(Vuesax)
Vue.use(ElementUI, { locale });
Vue.use(ToggleButton)
Vue.use(Vuelidate)
Vue.component('image-upload', ImageUpload);
Vue.mixin(Notify);
Vue.use(i18n);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue appcation instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('line-chart', {
    extends: VueChartJs.Line,
    mounted () {
      this.renderChart({
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        datasets: [
          {
            label: 'Data One',
            backgroundColor: '#f87979',
            data: [40, 39, 10, 40, 39, 80, 40]
          }
        ]
      }, {responsive: true, maintainAspectRatio: false})
    }
    
  })

  

const app = new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
});
if (!process.env.VUE_ENV)
    window.app = app;
export { app, router, store };
