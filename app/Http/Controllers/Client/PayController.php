<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cart;
use Auth,DB;
use Mail;
use App\Mail\SendMailPay;
use App\models\Bill\BillDetail;
use App\models\Bill\Bill;

class PayController extends Controller
{
    public function index()
    {
        
    	$data['cart'] = Cart::content();
        $data['total'] = (int)str_replace(',','',Cart::total());
		$data['cartcount'] = Cart::count();
        return view('pay.index',$data);
    }
    public function postPay(Request $request)
    {
    	$cart = Cart::content();
    	$input = $request->all();
        DB::beginTransaction();
			try {
				$billCode = rand();
				$query = new Bill();
				$query->code_bill = $billCode;
				if(Auth::guard('customer')->user() != null){
					$query->code_customer = Auth::guard('customer')->user()->id;
				}else{
					$query->code_customer = 0;
				}
				$query->total_money = (int)str_replace(',','',Cart::total());;
				$query->statu = 2;
				$query->note = $input['note'];
				$query->promotion = 0;
				$query->transport = "Giao hàng tận nơi";
				$query->transport_price = 40000;
				$query->cus_name = $input['name'];
				$query->cus_phone = $input['phone'];
				$query->cus_email = $input['email'];
				$query->cus_address = $input['address'];
				$query->save();
				foreach ($cart as $value) {
					$billDetail = new BillDetail();
					$billDetail->code_bill = $billCode;
					$billDetail->code_product = $value->options->code;
					$billDetail->qty = $value->qty;
					$billDetail->name = $value->name;
					$billDetail->price = $value->price;
					$billDetail->images = json_encode((Array)$value->options->img);
					$billDetail->discount = $value->options->discount;
					$billDetail->save();
				}
				DB::commit();
			} catch (\Throwable $e) {
			DB::rollBack();
			throw $e;
			}
    	Cart::destroy();
    	Mail::to($input['email'])->send(new SendMailPay($input));
    	return redirect(route('home'))->with('success','Thanh toán thành công');
    }
}
