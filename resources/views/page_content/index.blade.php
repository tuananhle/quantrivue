@extends('layouts.master')
@section('css')
<link href="{{asset('frontend/asset/css/evo-pages.scss')}}" rel="stylesheet" type="text/css" />
@endsection

@section('title') {{$pagecontentdetail->title}} @endsection
@section('description') {{$pagecontentdetail->description}} @endsection 
@section('url') {{ url('/page/'.$pagecontentdetail->slug) }} @endsection
@section('image') {{ $setting->logo }} @endsection

@section('content')
<section class="bread-crumb margin-bottom-10">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">         
          <li class="home"><a itemprop="url" href="/" title="Trang chủ"><span itemprop="title">Trang chủ</span></a><span><i class="fa fa-angle-right"></i></span></li>
          
          <li><strong itemprop="title">{{$pagecontentdetail->title}}</strong></li>
          
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="page">
   <div class="container">
      <div class="row">
         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="page-title category-title">
               <h1 class="title-head"><a href="javascript:;" title="Giới thiệu">{{$pagecontentdetail->title}}</a></h1>
            </div>
            <div class="content-page rte">
               {!! $pagecontentdetail->content !!}
            </div>
         </div>
      </div>
   </div>
</section>
@endsection