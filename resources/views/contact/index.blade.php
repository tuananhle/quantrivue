@extends('layouts.master')
@section('css')
<link href="{{asset('frontend/asset/css/evo-contacts.scss.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('title') Liên hệ @endsection
@section('description') Trang liên hệ @endsection 
@section('url'){{ route('contact.index') }} @endsection
@section('image') {{ $setting->logo }} @endsection

@section('content')
<section class="bread-crumb margin-bottom-10">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">					
					<li class="home"><a itemprop="url" href="/" title="Trang chủ"><span itemprop="title">Trang chủ</span></a><span><i class="fa fa-angle-right"></i></span></li>
					
					<li><strong itemprop="title">Liên hệ</strong></li>
					
				</ul>
			</div>
		</div>
	</div>
</section>
<div class="contact margin-bottom-20 page-contact margin-top-30">
   <div class="container">
      <div class="row contact-infos">
         <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="block-inner">
               <div class="icon">
                  <i class="fa fa-phone" aria-hidden="true"></i>
               </div>
               <div class="data">
                  <h4 class="block-title">Hotline</h4>
               <a href="tel:{{$setting->phone1}}" title="{{$setting->phone1}}">{{$setting->phone1}}</a>
               </div>
            </div>
         </div>
         <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="block-inner">
               <div class="icon">
                  <i class="fa fa-envelope" aria-hidden="true"></i>
               </div>
               <div class="data">
                  <h4 class="block-title">Email</h4>
               <a href="mailto:{{$setting->email}}" title="{{$setting->email}}">{{$setting->email}}</a>
               </div>
            </div>
         </div>
         <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="block-inner">
               <div class="icon">
                  <i class="fa fa-location-arrow" aria-hidden="true"></i>
               </div>
               <div class="data">
                  <h4 class="block-title">Địa chỉ</h4>
                  {{$setting->address1}}
               </div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-8 col-md-offset-2">
            <div class="page-login">
               <h1 class="title-head text-center">Gửi thông tin</h1>
               <span class="text-contact block text-center">Bạn hãy điền nội dung tin nhắn vào form dưới đây và gửi cho chúng tôi. Chúng tôi sẽ trả lời bạn sau khi nhận được.</span>
               <form accept-charset="UTF-8" action="/contact" id="contact" method="post" class="has-validation-callback">
                  <input name="FormType" type="hidden" value="contact">
                  <div class="form-signup clearfix">
                     <div class="row">
                        <div class="col-sm-6 col-xs-12">
                           <fieldset class="form-group">
                              <label>Họ và tên<span class="required">*</span></label>
                              <input placeholder="Nhập họ và tên" type="text" value="{{$profile ? $profile->name : "" }}" name="contact[name]" id="name" class="form-control  form-control-lg" data-validation-error-msg="Không được để trống" data-validation="required" required="">
                           </fieldset>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                           <fieldset class="form-group">
                              <label>Email<span class="required">*</span></label>
                              <input placeholder="Nhập địa chỉ Email" type="email" value="{{$profile ? $profile->email : "" }}" name="contact[email]" data-validation="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$" data-validation-error-msg="Email sai định dạng" id="email" class="form-control form-control-lg" required="">
                           </fieldset>
                        </div>
                        <div class="col-sm-12 col-xs-12">
                           <fieldset class="form-group">
                              <label>Điện thoại<span class="required">*</span></label>
                              <input placeholder="Nhập số điện thoại" type="tel" value="{{$profile ? $profile->phone : "" }}" name="contact[phone]" data-validation="tel" data-validation-error-msg="Không được để trống" id="tel" class="number-sidebar form-control form-control-lg" required="">
                           </fieldset>
                        </div>
                        <div class="col-sm-12 col-xs-12">
                           <fieldset class="form-group">
                              <label>Nội dung<span class="required">*</span></label>
                              <textarea placeholder="Nội dung liên hệ" name="contact[body]" id="comment" class="form-control form-control-lg" rows="5" data-validation-error-msg="Không được để trống" data-validation="required" required=""></textarea>
                           </fieldset>
                           <div class="pull-xs-left" style="margin-top:20px;">
                              <button type="submit" class="btn btn-blues btn-style btn-style-active">Gửi tin nhắn</button>
                           </div>
                        </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <div class="contact-map">
               <div class="page-login text-center">
                  <h3 class="title-head">Bản đồ cửa hàng</h3>
               </div>
               <div class="box-maps margin-bottom-10">
                  {!! $setting->iframe_map !!}
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection