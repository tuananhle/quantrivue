<section class="awe-section-10">
    <div class="container section_blogs">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="new_title">
                    <h2><a href="tin-tuc" title="Vào bếp">{{getLanguage('prize')}}</a></h2>
                    <p>{{getLanguage('the_results_that_we_have')}}</p>
                </div>
                <div class="evo-owl-blog evo-slick">
                    @foreach ($prize as $item)
                    <div class="news-items">
                    <a href="{{$item->link == null ? 'javascript:;' : $item->link}}"
                        title="{{$item->name}}"
                                class="clearfix evo-item-blogs">
                                <div class="evo-article-image">
                                    <img src="{{$item->image}}"
                                        data-src="{{$item->image}}"
                                        alt="{{$item->image}}"
                                        class="lazy img-responsive center-block" />
                                </div>
                                <h3 class="line-clamps">{{$item->name}}</h3>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>