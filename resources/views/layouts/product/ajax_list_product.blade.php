@foreach($data as $item)
<div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
        <div class="evo-product-block-item ">
        <div class="product-img">
            @if($item['discount'])
                <div class="product-sale" >
                <span>{{$item['discount']}}%</span>
                </div>
            @endif
            <a href="{{ route('product.detail', ['slug' => $item->slug]) }}" title="{{$item->name}}" class="image-resize">
            <img class="lazy loaded" src="{{json_decode($item->images)[0]}}" data-src="{{json_decode($item->images)[0]}}" alt="{{json_decode($item->images)[0]}}" data-was-processed="true">
            </a>
            <div class="button-add">
                    <div 
                            class="hidden-md variants form-nut-grid"
                            > 
                            <button type="button" title="Mua ngay">
                            <a style="color:#fff" href="{{route('cart.now',['id'=>$item->id])}}"
                            title="Mua ngay">Mua ngay</a>
                            </button>
                            <button class=""
                            data-toggle="modal" data-target="#popup-cart" data-id="{{$item->id}}">Thêm vào
                            giỏ</button>
                    </div>
            </div>
        </div>
        <div class="product-detail clearfix">
            <div class="box-pro-detail">
                <h3 class="pro-name">
                <a href="{{ route('product.detail', ['slug' => $item->slug]) }}" title="{{$item->name}}">{{$item->name}}</a>
                </h3>
                <div class="box-pro-prices">
                    @if($item['discount'])
                        <p class="pro-price">
                        {{number_format($item['price']-($item['price']*($item['discount']/100)))}}₫
                            <span class="pro-price-del">
                                <del class="compare-price">{{number_format($item['price'])}}</del>
                            </span>
                        </p>
                    @else
                        <p class="pro-price">
                            {{number_format($item['price'])}}₫
                        </p>
                    @endif
                </div>
            </div>
        </div>
        </div>
    </div>
@endforeach
<div class="clearfix"></div>
               <div class="text-xs-right">
                  <nav class="text-center">
                        {{ $data->links() }}
                  </nav>
               </div>