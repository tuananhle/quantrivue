@extends('layouts.master')
@section('css')
<link href="{{asset('frontend/asset/css/evo-article.scss.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('frontend/asset/css/evo-blogs.scss.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('title') Danh sách tin tức @endsection
@section('description') Cập nhật tin tức mới nhất từ Ribeto @endsection 
@section('url') {{ route('blog.list') }} @endsection
@section('image') {{ $setting->logo }} @endsection
@section('content')
<section class="bread-crumb margin-bottom-10">
   <div class="container">
      <div class="row">
         <div class="col-xs-12">
            <ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
               <li class="home"><a itemprop="url" href="/" title="Trang chủ"><span itemprop="title">Trang chủ</span></a><span><i class="fa fa-angle-right"></i></span></li>
               <li><strong itemprop="title">Tất cả tin tức</strong></li>
            </ul>
         </div>
      </div>
   </div>
</section>
<div class="container margin-top-20" itemscope="" itemtype="http://schema.org/Blog">
   <meta itemprop="name" content="Tất cả tin tức">
   <meta itemprop="description" content="Chủ đề không có mô tả">
   <div class="row">
      <div class="col-md-9 col-md-push-3 evo-list-blog-page">
         <h1 class="title-head">Tất cả tin tức</h1>
         <section class="evo-list-blogs row">
            @foreach($blog as $item)
            <article class="col-md-4 col-sm-6 col-xs-12 blog-item">
               <a href="{{route('blog.detail',['slug'=>$item->slug])}}" title="{{$item->title}}" class="clearfix">
                  <div class="evo-article-image">
                  <img style="max-height:200px;" src="{{$item->image}}" data-src="{{$item->image}}" alt="{{$item->title}}" class="lazy img-responsive center-block loaded" data-was-processed="true">
                  </div>
                <h3 class="line-clamp">{{$item->title}}</h3>
                  <p>{!! $item->description !!}
                  </p>
               </a>
            </article>
            @endforeach
            <div class="clearfix"></div>
            <div class="text-xs-right">
                <nav class="text-center">
                    {{ $blog->links() }}
                </nav>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12"></div>
         </section>
      </div>
      @include('layouts.sidebar')
   </div>
</div>
@endsection