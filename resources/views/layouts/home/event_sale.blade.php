<section class="awe-section-3">
    <div class="section_banner">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 banner-with-effects">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"
                        data-src="https://bizweb.dktcdn.net/100/360/151/themes/727143/assets/evo_banner_index_1.jpg"
                        alt="Thực phẩm sạch" class="img-responsive center-block lazy" />
                    <div class="figcaption">
                        {{-- <div class="banner-content">
                            <span class="small-title">Thực phẩm sạch</span><br>
                            <span class="big-title">Đồ ăn tươi ngon<br>Mỗi ngày</span>
                            <a href="#" class="btn_type " rel="nofollow" title="Xem thêm">Xem thêm</a>
                        </div> --}}
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 banner-with-effects">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"
                        data-src="https://bizweb.dktcdn.net/100/360/151/themes/727143/assets/evo_banner_index_2.jpg"
                        alt="Thực phẩm tươi" class="img-responsive center-block lazy" />
                    <div class="figcaption">
                        {{-- <div class="banner-content">
                            <span class="small-title">Thực phẩm tươi</span><br>
                            <span class="big-title">Giao nhanh <br>Chớp mắt</span>
                            <a href="#" class="btn_type btn_type_last" rel="nofollow" title="Xem thêm">Xem thêm</a>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>