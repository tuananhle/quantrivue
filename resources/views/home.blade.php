@extends('layouts.master')
@section('title') Ribeto | Chăm sóc sức khỏe cộng đồng @endsection
@section('description') {{$setting->fax}} @endsection 
@section('url') {{ $setting->webname }} @endsection
@section('image') {{ $setting->logo }} @endsection
@section('content')
    @include('layouts.home.banner')
    @include('layouts.home.box_product')
    @include('layouts.home.event_sale')
    @include('layouts.home.partner')
    <section class="awe-section-9">
        <div class="section_banner_dai">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <a href="#" title="RIBETO" class="single_image_effect">
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"
                                data-src="https://bizweb.dktcdn.net/100/360/151/themes/727143/assets/evo_banner_full_1.jpg"
                                alt="RIBETO" class="img-responsive center-block lazy" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('layouts.home.news')
    @include('layouts.home.prize')
@endsection