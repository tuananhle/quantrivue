<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\blog\Blog;
use Session;

class BlogController extends Controller
{
    public function list()
    {
        $language_current = Session::get('locale');
        $data['blog'] = Blog::where(['language'=>$language_current, 'status' => 1])->orderBy('id','DESC')->paginate(9);
        return view('blog.list',$data);
    }
    public function detail($slug)
    {
    	$language_current = Session::get('locale');
        $data['blog_detail'] = Blog::where(['language'=>$language_current, 'slug' => $slug])->first();
        return view('blog.detail',$data);
    }
}
