<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" class="anyflexbox boxshadow display-table">
   <head>
      <meta charset="utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="Evo Fresh - Thanh toán đơn hàng" />
      <title>Ribeto - Thanh toán đơn hàng</title>
      {{-- <link rel="shortcut icon" href="http://bizweb.dktcdn.net/100/360/151/themes/727143/assets/checkout_favicon.ico?1564021130230" type="image/x-icon" /> --}}
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" type="text/javascript"></script>
      <link href="http://bizweb.dktcdn.net/assets/themes_support/bootstrap.min.css?20171025" rel="stylesheet" type="text/css" />
      <link href="http://bizweb.dktcdn.net/assets/themes_support/nprogress.css?20171025" rel="stylesheet" type="text/css" />
      <link href="http://bizweb.dktcdn.net/assets/themes_support/font-awesome.min.css?20171025" rel="stylesheet" type="text/css" />
      <link href="http://bizweb.dktcdn.net/assets/themes_support/select2-min.css?20171025" rel="stylesheet" type="text/css" />
      <link href="http://bizweb.dktcdn.net/assets/themes_support/checkout.css?20190819" rel="stylesheet" type="text/css" />
      <link href="http://bizweb.dktcdn.net/100/360/151/checkout_stylesheet/727143/checkout.css?1564021130230" rel="stylesheet" type="text/css" />
   </head>
   <body class="body--custom-background-color ">
      <div class="banner">
         <div class="wrap">
            <div class="shop logo logo--center">
               <a href="/">
               <img class="logo__image  logo__image--medium " alt="Evo Fresh" src="{{$setting->logo}}"></img>
               </a>
            </div>
         </div>
      </div>
      <button class="order-summary-toggle">
         <div class="wrap">
            <h2>
               <label class="control-label">Đơn hàng</label>
               <label class="control-label hidden-small-device">
               ({{$cartcount}} sản phẩm)
               </label>
               <label class="control-label visible-small-device inline">
               ({{$cartcount}})
               </label>
            </h2>
            <a class="underline-none expandable pull-right" href="javascript:void(0)">
            Xem chi tiết
            </a>
         </div>
      </button>
      </div>
      <form method="post" action="" class="content stateful-form formCheckout">
         <div class="wrap" context="checkout">
            <div class='sidebar '>
               <div class="sidebar_header">
                  <h2>
                     <label class="control-label">Đơn hàng ({{$cartcount}} sản phẩm)</label>
                  </h2>
                  <hr class="full_width"/>
               </div>
               <div class="sidebar__content">
                  <div class="order-summary order-summary--product-list order-summary--is-collapsed">
                     <div class="summary-body summary-section summary-product" >
                        <div class="summary-product-list">
                           <table class="product-table">
                              <tbody>
                                 @foreach($cart as $item)
                                 <tr class="product product-has-image clearfix">
                                    <td>
                                       <div class="product-thumbnail">
                                          <div class="product-thumbnail__wrapper">
                                             <img src="{{$item->options->img ? $item->options->img : '' }}" class="product-thumbnail__image" />
                                          </div>
                                          <span class="product-thumbnail__quantity" aria-hidden="true">{{$item->qty}}</span>
                                       </div>
                                    </td>
                                    <td class="product-info">
                                       <span class="product-info-name">
                                       {{$item->name}}
                                       </span>
                                    </td>
                                    @if($item->discount)
                                    <td class="product-price text-right">
                                       {{number_format($item->price-($item->price*($item->discount/100)))}}₫
                                    </td>
                                    @else
                                    <td class="product-price text-right">
                                       {{number_format($item->price)}}₫
                                    </td>
                                    @endif
                                    
                                 </tr>
                                 @endforeach
                              </tbody>
                           </table>
                           <div class="order-summary__scroll-indicator">
                              Cuộn chuột để xem thêm
                              <i class="fa fa-long-arrow-down" aria-hidden="true"></i>
                           </div>
                        </div>
                     </div>
                     <hr class="m0"/>
                  </div>
                  
                  <div class="order-summary order-summary--total-lines">
                     <div class="summary-section border-top-none--mobile">
                        <div class="total-line total-line-subtotal clearfix">
                           <span class="total-line-name pull-left">
                           Tạm tính
                           </span>
                           <span  class="total-line-subprice pull-right">
                           {{number_format($total)}}₫
                           </span>
                        </div>
                        <div class="total-line total-line-shipping clearfix">
                           <span class="total-line-name pull-left">
                           Phí vận chuyển
                           </span>
                           <span class="total-line-subprice pull-right">
                           40,000₫
                           </span>
                        </div> 
                        <div class="total-line total-line-total clearfix">
                           <span class="total-line-name pull-left">
                           Tổng cộng:
                           </span>
                           <span class="total-line-subprice pull-right">
                           {{number_format($total+40000)}}₫
                           </span>
                        </div>
                     </div>
                  </div>
                  <div class="form-group has-error">
                     <div class="help-block ">
                        <ul class="list-unstyled">
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <div class="main" role="main">
               <div class="main_header">
                  <div class="shop logo logo--center">
                     <a href="/">
                     <img class="logo__image  logo__image--medium " alt="Evo Fresh" src="{{$setting->logo}}"></img>
                     </a>
                  </div>
               </div>
               <div class="main_content">
                  <form action="{{route('pay.post')}}" method="post">
                     @csrf
                  <input type="hidden" name="cart" value="{{$cart}}">
                        <div class="row">
                           <div class="col-md-6 col-lg-6">
                              <div class="section" define="{billing_address: {}}">
                                 <div class="section__header">
                                    <div class="layout-flex layout-flex--wrap">
                                       <h2 class="section__title layout-flex__item layout-flex__item--stretch">
                                          <i class="fa fa-id-card-o fa-lg section__title--icon hidden-md hidden-lg" aria-hidden="true"></i>
                                          <label class="control-label">Thông tin mua hàng</label>
                                       </h2>
                                       @if(!$profile)
                                    <a class="layout-flex__item section__title--link" href="{{route('login.index')}}">
                                       <i class="fa fa-user-circle-o fa-lg" aria-hidden="true"></i>
                                       Đăng nhập 
                                       </a>
                                       @endif
                                    </div>
                                 </div>
                                 <div class="section__content">
                                    <div class="form-group">
                                       <div>
                                          <label class="field__input-wrapper">
                                          <input name="email" type="email" placeholder="Email" value="{{$profile ? $profile->email : "" }}"  class="field__input form-control" id="_email" required />
                                          </label>
                                       </div>
                                       <div class="help-block with-errors">
                                       </div>
                                    </div>
                                    <div class="billing">
                                       <div>
                                          <div class="form-group">
                                             <div class="field__input-wrapper">
                                                <input name="name" placeholder="Họ và tên" value="{{$profile ? $profile->name : "" }}" type="text" class="field__input form-control" id="_billing_address_last_name" required  />
                                             </div>
                                             <div class="help-block with-errors"></div>
                                          </div>
                                          <div class="form-group">
                                             <div class="field__input-wrapper">
                                                <input name="phone" placeholder="Số điện thoại" value="{{$profile ? $profile->phone : "" }}" class="field__input form-control" id="_billing_address_phone"/>
                                             </div>
                                             <div class="help-block with-errors"></div>
                                          </div>
                                          <div class="form-group">
                                             <div class="field__input-wrapper" >
                                                <input name="address" placeholder="Địa chỉ" class="field__input form-control" id="_billing_address_address1" />
                                             </div>
                                             <div class="help-block with-errors"></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="section pt10">
                                 <div class="section__header">
                                    <h2 class="section__title">
                                       <i class="fa fa-id-card-o fa-lg section__title--icon hidden-md hidden-lg" aria-hidden="true"></i>
                                       <label class="control-label">
                                       Thông tin nhận hàng
                                       </label>
                                    </h2>
                                 </div>
                              </div>
                              <div class="section">
                                 <div class="section__content">
                                    <div class="form-group m0">
                                       <div>
                                          <label class="field__input-wrapper" style="border: none">
                                          <textarea name="note" placeholder="Ghi chú" 
                                             class="field__input form-control m0"></textarea>
                                          </label>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-6 col-lg-6">
                              <div class="section shipping-method">
                                 <div class="section__header">
                                    <h2 class="section__title">
                                       <i class="fa fa-truck fa-lg section__title--icon hidden-md hidden-lg" aria-hidden="true"></i>
                                       <label class="control-label">Vận chuyển</label>
                                    </h2>
                                 </div>
                                 <div class="section__content">
                                    <div class="content-box" bind-show="!shippingMethodsLoading && shippingMethods.length > 0">
                                       <div class="content-box__row"><div class="radio-wrapper"><div class="radio__input"><input class="input-radio" checked type="radio" value="516358,0" name="ShippingMethod" id="shipping_method_516358" bind="shippingMethod" bind-event-change="changeShippingMethod()" fee="40000"></div><label class="radio__label" for="shipping_method_516358"> <span class="radio__label__primary">Giao hàng tận nơi</span><span class="radio__label__accessory"><span class="content-box__emphasis">40.000₫</span></span></label> </div> <!-- /radio-wrapper--> </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="section payment-methods">
                                 <div class="section__header">
                                    <h2 class="section__title">
                                       <i class="fa fa-credit-card fa-lg section__title--icon hidden-md hidden-lg" aria-hidden="true"></i>
                                       <label class="control-label">Thanh toán</label>
                                    </h2>
                                 </div>
                                 <div class="section__content">
                                    <div class="content-box">
                                       <div class="content-box__row">
                                          <div class="radio-wrapper">
                                             <div class="radio__input">
                                                <input class="input-radio" type="radio" value="428557" name="PaymentMethodId" id="payment_method_428557" checked>
                                             </div>
                                             <label class="radio__label" for="payment_method_428557">
                                                <span class="radio__label__primary">Thanh toán khi giao hàng (COD)</span>
                                                <span class="radio__label__accessory">
                                                   <ul>
                                                      <li class="payment-icon-v2 payment-icon--4">
                                                         <i class="fa fa-money payment-icon-fa" aria-hidden="true"></i>
                                                      </li>
                                                   </ul>
                                                </span>
                                             </label>
                                          </div>
                                       </div>
                                       <div class="radio-wrapper content-box__row content-box__row--secondary hide" id="payment-gateway-subfields-428557">
                                          <div class="blank-slate">
                                             <p>Bạn chỉ phải thanh toán khi nhận được hàng</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="section">
                                 <div class="form-group clearfix m0">
                                    <button class="btn btn-primary btn-checkout" type="submit">ĐẶT HÀNG</button>
                                 </div>
                                 <div class="text-center mt20">
                                    <a class="previous-link" href="/cart">
                                    <i class="fa fa-angle-left fa-lg" aria-hidden="true"></i>
                                    <span>Quay về giỏ hàng</span>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                  </form>
               </div>
               <div class="main_footer footer unprint">
                  <div class="mt10"></div>
               </div>
            </div>
         </div>
      </form>
      <script src="http://bizweb.dktcdn.net/assets/themes_support/bootstrap.min.js?20171025" type="text/javascript"></script>
      <script src="http://bizweb.dktcdn.net/assets/themes_support/twine.min.js?20171025" type="text/javascript"></script>
      <script src="http://bizweb.dktcdn.net/assets/themes_support/nprogress.js?20171025" type="text/javascript"></script>
      <script src="http://bizweb.dktcdn.net/assets/themes_support/money-helper.js?20171025" type="text/javascript"></script>
      <script src="http://bizweb.dktcdn.net/assets/themes_support/select2-full-min.js?20171025" type="text/javascript"></script>
      <script src="http://bizweb.dktcdn.net/assets/scripts/ua-parser.pack.js?20180611" type='text/javascript'></script>
      <script src="http://bizweb.dktcdn.net/assets/themes_support/checkout.js?20190820" type="text/javascript"></script>
      <script type="text/javascript">

      </script>
   </body>
</html>