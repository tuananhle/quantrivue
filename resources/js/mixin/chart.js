import { Line, mixins } from 'vue-chartjs'
import { mapActions, mapState } from 'vuex';
const { reactiveProp } = mixins
export default {
  extends: Line,
  mixins: [reactiveProp],
  data(){
    return {
      day:"",
    }
  },
  props: ['dayForChart','moneyForChart','chartData','options'],
  methods:{
    ...mapActions(['getDataChart']),
    dateChart(){
      return this.dayForChart;
    },
    resetOrShowData(){
      this.renderChart({
        labels: this.dateChart,
        datasets: [
          {
            label: 'Data One',
            backgroundColor: '#f87979',
            data: [40, 39, 10, 40, 39, 80, 40]
          }
        ]
      }, {responsive: true, maintainAspectRatio: false}
    )
    }
  },
  mounted () {
    // this.chartData is created in the mixin.
    // If you want to pass options please create a local options object
    this.$nextTick(function () {      
      console.log(this.dayForChart);
   });
    
    
  }
}