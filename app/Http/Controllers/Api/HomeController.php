<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Bill\Bill;
use Carbon\Carbon;

class HomeController extends Controller
{
    public function chart(Request $request)
    {
        $dayStart = $request->dateStart && $request->dateEnd ? $request->dateStart : Carbon::now()->subDay(6)->toDateString();
        $dayEnd = $request->dateStart && $request->dateEnd ? $request->dateEnd : Carbon::now()->toDateString();

        $start = strtotime($dayStart);
        $end1 = strtotime($dayEnd);
        $interval   = 1*24*60*60;
        
        $chunks = array();
        for($time=$start; $time<=$end1; $time+=$interval){
            $chunks[] = date('Y-m-d', $time);
        }
        $day = [];
        $monney = [];
        foreach($chunks as $item){
            $billForDay = Bill::where('updated_at', 'like', '%' . $item . '%')
            ->get();
            $monteyDay = $billForDay->sum('total_money');
            $day[] = $item;
            $monney[] = $monteyDay;
        }
        return response()->json([
            'monney'=> $monney,
            'day'=> $day,
            'message' => 'success'
        ]);
    }
}
