<?php

namespace App\models\blog;

use Illuminate\Database\Eloquent\Model;
use App\models\language\Language;
use Auth;

class Blog extends Model
{
    protected $table = "blogs";
    public function saveBlog($request)
    {
    	$quiz_id = $request->quiz_id;
        $language = $request->language;
        $nextId = Blog::max('id') + 1;
        if($quiz_id != "" && $language != ""){
            $query = Blog::where([
                'quiz_id' => $quiz_id,
                'language'=> $language
             ])->first();
            if ($query) {
                $file= str_replace('http://localhost:8080','',$query->image);
                $filename = public_path().$file;
                if(file_exists( public_path().$file ) ){
                    \File::delete($filename);
                }
                $query->title = $request->title;
                $query->content = $request->content;
                $query->description = $request->description;
                $query->status = $request->status;
                $query->slug = to_slug($request->title);
                $query->author = Auth::user()->name;
                $query->save();
            }else{
                $query = new Blog();
                $query->quiz_id = $quiz_id;
                $query->language = $language;
                $query->title = $request->title;
                $query->content = $request->content;
                $query->image = $request->image;
                $query->description = $request->description;
                $query->status = $request->status;
                $query->slug = to_slug($request->title);
                $query->author = Auth::user()->name;
                $query->save();
            }
            
        }else{
            $listLanguage = Language::get()->toArray();
            foreach ($listLanguage as $item) {
                $query = new Blog();
                $query->quiz_id = $nextId;
                $query->language = $item['code'];
                $query->title = $request->title;
                $query->content = $request->content;
                $query->image = $request->image;
                $query->description = $request->description;
                $query->status = $request->status;
                $query->slug = to_slug($request->title);
                $query->author = Auth::user()->name;
                $query->save();
            }
            
        }
        return $query;
    }
    public function deleteBlog($quiz_id)
    {
        $query = Blog::where('quiz_id',$quiz_id)->get();
        foreach($query as $item){
            $data = Blog::find($item->id);
            $file= str_replace('http://localhost:8080','',$data->image);
            $filename = public_path().$file;
            if(file_exists( public_path().$file ) ){
                \File::delete($filename);
            }
            $data->delete();
        }
        
    }
}
