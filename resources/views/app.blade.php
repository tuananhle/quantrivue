<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CMS-phầm mềm quản trị</title>
        <link href="{{ mix('css/style.css') }}" type="text/css" rel="stylesheet" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script>
        window.__ENV__ = window.__ENV || {}; __ENV__.link ='http://127.0.0.1:8000/';
        window.Laravel = {!! 
            json_encode([
                'csrf_token' => csrf_token(),
            ])
         !!};
        </script>
    </head>
    <body>
        <div id="app">
        </div>
        <script src="{{ mix('js/app.js') }}" type="text/javascript"></script>
    </body>
    <script type="text/javascript">
        
    </script>
</html>