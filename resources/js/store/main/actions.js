// import axios from '../../core/plugins/http'
import CONSTANTS from '../../core/utils/constants';
import {HTTP} from '../../core/plugins/http'

export const loadings = (context, bol) => {
        context.commit('SET_LOADING', bol);
};

export const saveBanner  = ({commit},opt) => {
    return new Promise((resolve, reject) => {
        HTTP.post('/api/website/banner', opt).then(response => {
            return resolve(response.data);
        }).catch(error => {
            return reject(error);
        })
    });
};
export const listBanner  = ({commit},opt) => {
    return new Promise((resolve, reject) => {
        HTTP.get('/api/website/list-banner', opt).then(response => {
            return resolve(response.data);
        }).catch(error => {
            return reject(error);
        })
    });
};
export const savePartner  = ({commit},opt) => {
    return new Promise((resolve, reject) => {
        HTTP.post('/api/website/partner', opt).then(response => {
            return resolve(response.data);
        }).catch(error => {
            return reject(error);
        })
    });
};
export const listPartner  = ({commit},opt) => {
    return new Promise((resolve, reject) => {
        HTTP.get('/api/website/list-partner', opt).then(response => {
            return resolve(response.data);
        }).catch(error => {
            return reject(error);
        })
    });
};

export const listPrize  = ({commit},opt) => {
    return new Promise((resolve, reject) => {
        HTTP.get('/api/website/list-prize', opt).then(response => {
            return resolve(response.data);
        }).catch(error => {
            return reject(error);
        })
    });
};
export const savePrize  = ({commit},opt) => {
    return new Promise((resolve, reject) => {
        HTTP.post('/api/website/prize', opt).then(response => {
            return resolve(response.data);
        }).catch(error => {
            return reject(error);
        })
    });
};
export const getDataChart = ({commit},opt) => {
    return new Promise((resolve, reject) => {
        HTTP.post('/api/home/chart', opt).then(response => {
            return resolve(response.data);
        }).catch(error => {
            return reject(error);
        })
    });
}