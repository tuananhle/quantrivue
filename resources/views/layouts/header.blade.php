<header class="header"> 
    <div class="evo-top-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12 slogan">
                    <p>{{$setting->company}} | {{getLanguage('social_health_care')}}</p>
                </div>
                <div class="col-md-6 col-sm-6 evo-account hidden-xs hidden-sm">
                    <ul>
                        @php 
                            $languages = getActiveLanguages();
                        @endphp
                        @if(!empty($languages))
                            @foreach($languages as $item)
                            <li>
                                    <form action="/languages" method="POST" enctype="multipart/form-data"
                                    id="form-language" style="margin-bottom: 0;">
                                    @csrf
                                        <input type="hidden" name="code" value="{{ $item['code'] }}"/>
                                        <button class="btn btn-link btn-block language-select" type="submit">
                                                <img width=30 height="25" 
                                                src="{{ $item['flagIcon'] }}"
                                                alt="{{ $item['name'] }}"
                                                title="{{ $item['name'] }}"/>
                                        </button>
                                    </form>
                            </li>
                            @endforeach
                        @endif
                        @if(Auth::guard('customer')->user() != null)
                        <li><a rel="nofollow" href="/account" title="{{Auth::guard('customer')->user()->name}}"><i class="fa fa-user" aria-hidden="true"></i> {{getLanguage('hello')}} {{Auth::guard('customer')->user()->name}}</a></li>
                        <li><a rel="nofollow" href="{{route('logout.index')}}" title="Đăng xuất"><i class="fa fa-sign-out" aria-hidden="true"></i> {{getLanguage('logut')}}</a></li>
                        @else
                        <li><a rel="nofollow" href="{{route('login.index')}}" title="Đăng nhập"><i class="fa fa-user-plus"
                            aria-hidden="true"></i>{{getLanguage('login')}}</a></li>
                        <li><a rel="nofollow" href="{{route('register.index')}}" title="Đăng ký"><i class="fa fa-user-plus"
                            aria-hidden="true"></i> {{getLanguage('register')}}</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="evo-header-logo-search-cart">
        <div class="container">
            <div class="row">
                <div class="col-md-3 logo evo-header-mobile">
                    <button type="button"
                        class="evo-flexitem evo-flexitem-fill navbar-toggle collapsed visible-sm visible-xs"
                        id="trigger-mobile">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="/" class="logo-wrapper" title="RIBETO">
                        <img src="{{$setting->logo}}" alt="RIBETO" class="lazy img-responsive center-block" />
                    </a>
                    <div class="evo-flexitem evo-flexitem-fill visible-sm visible-xs">
                        <a href="javascript:void(0);" class="site-header-search" rel="nofollow" title="Tìm kiếm">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </a>
                        <a href="/cart" title="Giỏ hàng" rel="nofollow">
                            <svg viewBox="0 0 100 100" data-radium="true" style="width: 25px;">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g transform="translate(-286.000000, -515.000000)" fill="#fff">
                                        <path
                                            d="M374.302082,541.184324 C374.044039,539.461671 372.581799,538.255814 370.861517,538.255814 L351.078273,538.255814 L351.078273,530.159345 C351.078273,521.804479 344.283158,515 335.93979,515 C327.596422,515 320.801307,521.804479 320.801307,530.159345 L320.801307,538.255814 L301.018063,538.255814 C299.297781,538.255814 297.835541,539.461671 297.577499,541.184324 L286.051608,610.951766 C285.87958,611.985357 286.137623,613.018949 286.825735,613.794143 C287.513848,614.569337 288.460003,615 289.492173,615 L382.387408,615 L382.473422,615 C384.451746,615 386,613.449612 386,611.468562 C386,611.037898 385.913986,610.693368 385.827972,610.348837 L374.302082,541.184324 L374.302082,541.184324 Z M327.854464,530.159345 C327.854464,525.680448 331.467057,522.062877 335.93979,522.062877 C340.412524,522.062877 344.025116,525.680448 344.025116,530.159345 L344.025116,538.255814 L327.854464,538.255814 L327.854464,530.159345 L327.854464,530.159345 Z M293.62085,608.023256 L304.028557,545.318691 L320.801307,545.318691 L320.801307,565.043066 C320.801307,567.024117 322.349561,568.574505 324.327886,568.574505 C326.30621,568.574505 327.854464,567.024117 327.854464,565.043066 L327.854464,545.318691 L344.025116,545.318691 L344.025116,565.043066 C344.025116,567.024117 345.57337,568.574505 347.551694,568.574505 C349.530019,568.574505 351.078273,567.024117 351.078273,565.043066 L351.078273,545.318691 L367.851024,545.318691 L378.25873,608.023256 L293.62085,608.023256 L293.62085,608.023256 Z">
                                        </path>
                                    </g>
                                </g>
                            </svg>
                        <span class="count_item_pr">{{$cartcounts}}</span>
                        </a>
                    </div>
                </div>
                <div class="col-md-6 evo-header-search hidden-sm hidden-xs">
                    <form action="/search" method="get">
                        <div class="input-group">
                            <input type="text" name="query" class="search-auto form-control"
                                placeholder="Bạn cần tìm gì...?" />
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
                <div class="col-md-3 evo-header-hotline-cart hidden-sm hidden-xs">
                    <div class="hotline">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <a href="tel:0900090090" title="Hotline 24/7 0900090090">
                            <span class="evo-title">Hotline 24/7</span>
                            <span class="evo-hotline">{{$setting->phone1}}</span>
                        </a>
                    </div>
                    <div class="evo-cart mini-cart" id="top-cart-content">
                        <a href="/cart" title="Giỏ hàng" rel="nofollow">
                            <svg viewBox="0 0 100 100" data-radium="true" style="width: 25px;">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g transform="translate(-286.000000, -515.000000)" fill="#e91b23">
                                        <path
                                            d="M374.302082,541.184324 C374.044039,539.461671 372.581799,538.255814 370.861517,538.255814 L351.078273,538.255814 L351.078273,530.159345 C351.078273,521.804479 344.283158,515 335.93979,515 C327.596422,515 320.801307,521.804479 320.801307,530.159345 L320.801307,538.255814 L301.018063,538.255814 C299.297781,538.255814 297.835541,539.461671 297.577499,541.184324 L286.051608,610.951766 C285.87958,611.985357 286.137623,613.018949 286.825735,613.794143 C287.513848,614.569337 288.460003,615 289.492173,615 L382.387408,615 L382.473422,615 C384.451746,615 386,613.449612 386,611.468562 C386,611.037898 385.913986,610.693368 385.827972,610.348837 L374.302082,541.184324 L374.302082,541.184324 Z M327.854464,530.159345 C327.854464,525.680448 331.467057,522.062877 335.93979,522.062877 C340.412524,522.062877 344.025116,525.680448 344.025116,530.159345 L344.025116,538.255814 L327.854464,538.255814 L327.854464,530.159345 L327.854464,530.159345 Z M293.62085,608.023256 L304.028557,545.318691 L320.801307,545.318691 L320.801307,565.043066 C320.801307,567.024117 322.349561,568.574505 324.327886,568.574505 C326.30621,568.574505 327.854464,567.024117 327.854464,565.043066 L327.854464,545.318691 L344.025116,545.318691 L344.025116,565.043066 C344.025116,567.024117 345.57337,568.574505 347.551694,568.574505 C349.530019,568.574505 351.078273,567.024117 351.078273,565.043066 L351.078273,545.318691 L367.851024,545.318691 L378.25873,608.023256 L293.62085,608.023256 L293.62085,608.023256 Z">
                                        </path>
                                    </g>
                                </g>
                            </svg>
                            <span class="count_item_pr">{{$cartcounts}}</span>
                        </a>
                        <div class="top-cart-content">
                            {{-- @if(count($cart_content) > 0) --}}
                            <ul id="cart-sidebar" class="mini-products-list count_li">
                                <li class="list-item">
                                    <ul class="list-item-cart">
                                        @foreach ($cart_content as $item)
                                            <li class="item productid-25835028">
                                                <a class="product-image" href="{{ route('product.detail', ['slug' => $item->options->slug]) }}" title="{{$item->name}}"><img alt="{{$item->name}}" src="{{$item->options->img ? $item->options->img : '' }}" width="80"></a>
                                                <div class="detail-item">
                                                    <div class="product-details">
                                                        <a href="javascript:;"  title="Xóa" onclick="deleteCart('{{$item->rowId}}')" class="fa fa-remove">&nbsp;</a>
                                                        <p class="product-name"> <a href="{{ route('product.detail', ['slug' => $item->options->slug]) }}" title="{{$item->name}}">{{$item->name}}</a></p>
                                                    </div>
                                                    <div class="product-details-bottom">
                                                            @if($item->discount)
                                                                <span class="price pricechange">{{number_format($item->price-($item->price*($item->discount/100)))}}₫</span>
                                                            @else
                                                                <span class="price pricechange">{{number_format($item->price)}}₫</span>
                                                            @endif
                                                        <div class="quantity-select">
                                                            <button onclick="downQty('qtyItem{{$item->options->code}}','{{$item->rowId}}',{{$item->id}})"  class="reduced items-counts btn-minus" type="button">–</button>
                                                            <input type="text" disabled="" maxlength="3" min="1" class="input-text number-sidebar" id="qtyItem{{$item->options->code}}" name="Lines" size="4" value="{{$item->qty}}">
                                                            <button onclick="upQty('qtyItem{{$item->options->code}}','{{$item->rowId}}',{{$item->id}})" class="increase items-counts btn-plus" type="button">+</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                                <li class="action">
                                    <ul>
                                        <li class="li-fix-1">
                                            <div class="top-subtotal">{{getLanguage('total_money')}}: {{$totalcart}}<span
                                                    class="price"></span></div>
                                        </li>
                                        <li class="li-fix-2">
                                            <div class="actions clearfix">
                                            <a rel="nofollow" href="{{route('cart.list')}}" class="btn btn-primary"
                                                    title="Giỏ hàng"><i class="fa fa-shopping-basket"></i> {{getLanguage('cart')}}</a>
                                            <a rel="nofollow" href="{{route('pay.index')}}" class="btn btn-checkout btn-gray"
                                                    title="Thanh toán"><i class="fa fa-random"></i> {{getLanguage('pay')}}</a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            {{-- @else 
                            <ul id="cart-sidebar" class="mini-products-list count_li">
                                <li class="list-item">
                                    <p>Chưa có sản phẩm nào</p>
                                </li>
                            </ul>
                            @endif --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="evo-main-nav">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="mobile-main-menu hidden-lg hidden-md">
                        <div class="drawer-header">
                            <div id="close-nav">
                                <svg viewBox="0 0 100 100" data-radium="true" style="width: 16px;">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g transform="translate(-645.000000, -879.000000)" fill="#000">
                                            <path
                                                d="M743.998989,926.504303 L697.512507,880.032702 C696.909905,879.430293 695.962958,879 695.016011,879 C694.069064,879 693.122117,879.430293 692.519515,880.032702 L646.033033,926.504303 C644.655656,927.881239 644.655656,930.118761 646.033033,931.495697 C646.721722,932.184165 647.582582,932.528399 648.529529,932.528399 C649.476476,932.528399 650.337337,932.184165 651.026025,931.495697 L691.486482,891.048193 L691.486482,975.471601 C691.486482,977.450947 693.036031,979 695.016011,979 C696.995991,979 698.54554,977.450947 698.54554,975.471601 L698.54554,891.048193 L739.005997,931.495697 C740.383374,932.872633 742.621612,932.872633 743.998989,931.495697 C745.376366,930.118761 745.29028,927.881239 743.998989,926.504303 L743.998989,926.504303 Z"
                                                transform="translate(695.000000, 929.000000) rotate(-90.000000) translate(-695.000000, -929.000000) ">
                                            </path>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                            <a href="/" class="logo-wrapper" title="RIBETO">
                                <img src="./logo.png" alt="RIBETO" class="lazy img-responsive center-block" />
                            </a>
                        </div> 
                        <div class="ul-first-menu">
                            <a rel="nofollow" href="/account/login" title="Đăng nhập">{{getLanguage('login')}}</a>
                            <a rel="nofollow" href="/account/register" title="Đăng ký">{{getLanguage('register')}}</a>
                        </div>
                    </div>
                    <ul id="nav" class="nav">
                        <li class="nav-item active"><a class="nav-link" href="/" title="Trang chủ">{{getLanguage('Trangchu')}}</a>
                        </li>
                        
                        <li class=" nav-item has-childs  has-mega">
                        <a href="{{ route('product.list') }}" class="nav-link" title="Sản phẩm">{{getLanguage('product')}} <i
                                    class="fa fa-angle-down" data-toggle="dropdown"></i></a>
                            <div class="mega-content">
                                <ul class="level0">
                                    <li class="level1 parent item fix-navs">
                                        <a class="hmega" href="{{ route('product.list') }}" title="Đồ uống các loại">{{getLanguage('productsus')}}<i class="fa fa-angle-down hidden-lg hidden-md"
                                                data-toggle="dropdown"></i></a>
                                        <ul class="level1">
                                            @foreach($product_menu as $item)
                                            <li class="level2">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <a href="{{ route('product.detail', ['slug' => $item->slug]) }}">
                                                            <div class="pro-menu">
                                                                <img src="{{json_decode($item->images)[0]}}" alt="">
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <a href="{{ route('product.detail', ['slug' => $item->slug]) }}" title="{{$item->name}}">{{$item->name}}</a>
                                                    </div>
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item "><a class="nav-link" href="{{ route('blog.list') }}" title="Tin tức">{{getLanguage('news')}}</a></li>
                        @foreach($pagecontent as $item)
                        <li class="nav-item "><a class="nav-link" href="{{route('pagecontent.detail',['slug'=>$item->slug])}}" title="{{$item->title}}">{{$item->title}}</a></li>
                        @endforeach
                        <li class="nav-item "><a class="nav-link" href="/fukujyusen" title="Fukujyusen">Fukujyusen</a>
                        </li>
                        <li class="nav-item "><a class="nav-link" href="{{route('contact.index')}}" title="Liên hệ">{{getLanguage('contact')}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<script>
    
</script>