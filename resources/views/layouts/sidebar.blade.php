<aside class="evo-sidebar sidebar left-content col-md-3 col-md-pull-9">
        <aside class="aside-item collection-category">
           <div class="aside-title">
              <h3 class="title-head margin-top-0">Danh mục</h3>
           </div>
           <div class="aside-content">
              <ul class="nav navbar-pills nav-category">
                 <li class="nav-item ">
                    <a class="nav-link" href="/" title="Trang chủ">Trang chủ</a>
                 </li>
                 <li class="nav-item ">
                    <a class="nav-link" href="/gioi-thieu" title="Giới thiệu">Giới thiệu</a>
                 </li>
                 <li class="nav-item active">
                    <a href="{{ route('product.list') }}" class="nav-link" title="Sản phẩm">Sản phẩm</a>
                    <span class="Collapsible__Plus"></span>
                    <ul class="dropdown-menu">
                      @foreach($product_menu as $item)
                        <li class="dropdown-submenu nav-item ">
                          <a class="nav-link" href="{{ route('product.detail', ['slug' => $item->slug]) }}" title="{{$item->name}}">{{$item->name}}</a>
                        </li>
                      @endforeach
                    </ul>
                 </li>
                 <li class="nav-item ">
                    <a class="nav-link" href="{{ route('blog.list') }}" title="Khuyến mãi">Tin tức</a>
                 </li>
                 @foreach($pagecontent as $item)
                 <li class="nav-item ">
                    <a class="nav-link" href="{{route('pagecontent.detail',['slug'=>$item->slug])}}" title="{{$item->title}}">{{$item->title}}</a>
                 </li>
                 @endforeach
                 <li class="nav-item ">
                    <a class="nav-link" href="{{route('contact.index')}}" title="Liên hệ">Liên hệ</a>
                 </li>
              </ul>
           </div>
        </aside>
     </aside>