function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}
$('#popup-cart').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var pro_id = button.data('id')
    var modal = $(this)
    var url =  "/cart/create" + '/' + pro_id
    $.ajax({
        type: 'get',
        dataType: 'json',
        url: url,
        success:function(response){
                const cartArray = Object.keys(response.cart).map(i => response.cart[i])
                let html = "";
                let html2 = "";
                let html3 = "";
                cartArray.forEach(function(i){
                                html+='<div class="item-popup itemid-25835028">'
                                html+=    '<div style="width: 55%;" class="text-left">'
                                html+=        '<div class="item-image">'
                                html+=        '<a class="item-image" href="" title="'+i.name+'"><img alt="'+i.name+'" src="'+ i.options.img + '" width="80" /></a>'
                                html+=    '</div>'
                                html+=    '<div class="item-info">'
                                html+=        '<p class="item-name"><a href="#" title="'+i.name+'">'+i.name+'</a></p>'
                                html+=        '<p class="variant-title-popup"></p>'
                                html+=        '<p class="item-remove"><a href="javascript:;" onclick="deleteCart(\'' + i.rowId + '\')"' + 'title="Xóa"><i class="fa fa-close"></i> Bỏ sản phẩm</a></p>'
                                html+=    '</div>'
                                html+='</div>'
                                html+='<div style="width: 15%;" class="text-right">'
                                    if(i.discount){
                                html+='<div class="item-price"><span class="price pricechange">'+(i.price-(i.price*(i.discount/100))).toFixed(3)+'₫</span></div>'
                                    }else{
                                html+='<div class="item-price"><span class="price pricechange">'+i.price+'₫</span></div>'
                                    }
                                html+='</div>'
                                html+='<div style="width: 15%;" class="text-center">'
                                html+='<button onclick="downQty(\'qtyItem'+i.options.code+ '\',\'' + i.rowId + '\',' + i.id + ')"' + 'class="reduced items-counts btn-minus" type="button">–</button>'
                                html+='<input disabled="" type="text" maxlength="12" min="0"  id="qtyItem'+i.options.code+'" name="Lines" class="input-text number-sidebar" size="4" value="' + i.qty+ '">'
                                html+='<button onclick="upQty(\'qtyItem'+i.options.code+ '\',\'' + i.rowId + '\',' + i.id + ')"' + 'class="increase items-counts btn-minus" type="button">+</button>'
                                html+='</div>'
                                html+='<div style="width: 15%;" class="text-right"><span class="cart-price"> <span class="price">'+i.price*i.qty+'₫</span> </span>'
                                html+='</div>'
                                html+='</div>'
                                html2+= '<li class="item productid-25835028">'
                                html2+=    '<a class="product-image" href="" title="'+i.name+'"><img alt="'+i.name+'" src="'+ i.options.img + '" width="80"></a>'
                                html2+=    '<div class="detail-item">'
                                html2+=        '<div class="product-details">'
                                html2+=            '<a href="javascript:;"  title="Xóa" onclick="deleteCart(\'' + i.rowId + '\')"' + ' class="fa fa-remove">&nbsp;</a>'
                                html2+=            '<p class="product-name"> <a href="#" title=""'+i.name+'"">'+i.name+'</a></p>'
                                html2+=        '</div>'
                                html2+=        '<div class="product-details-bottom">'
                                                      if(i.discount){
                                html2+=                    '<span class="price pricechange">'+(i.price-(i.price*(i.discount/100))).toFixed(3)+'₫</span>'
                                                        }else{
                                html2+=                    '<span class="price pricechange">'+i.price+'₫</span>'
                                                      }
                                html2+=            '<div class="quantity-select">'
                                html2+=                '<button onclick="downQty(\'qtyItem'+i.options.code+ '\',\'' + i.rowId + '\',' + i.id + ')"' + 'class="reduced items-counts btn-minus" type="button">–</button>'
                                html2+=                '<input type="text" disabled="" maxlength="3" min="1" class="input-text number-sidebar"  name="Lines" size="4" value="' + i.qty+ '">'
                                html2+=                '<button onclick="upQty(\'qtyItem'+i.options.code+ '\',\'' + i.rowId + '\',' + i.id + ')"' + 'class="increase items-counts btn-plus" type="button">+</button>'
                                html2+=            '</div>'
                                html2+=        '</div>'
                                html2+=    '</div>'
                                html2+='</li>'
                                html3+='<div class="row shopping-cart-item productid-25835028">'
                                html3+=    '<div class="col-xs-3 img-thumnail-custom">'
                                html3+=        '<p class="image"><a href="#" title="'+i.name+'" target="_blank"><img class="img-responsive" src="'+ i.options.img + '" alt="'+i.name+'"></a></p>'
                                html3+=    '</div>'
                                html3+=    '<div class="col-right col-xs-9">'
                                html3+=        '<div class="box-info-product">'
                                html3+=            '<p class="name"><a href="#" title="'+i.name+'" target="_blank">'+i.name+'</a></p>'
                                html3+=            '<p class="action"><a href="javascript:;" onclick="deleteCart(\'' + i.rowId + '\')"' + ' class="btn btn-link btn-item-delete" title="Xóa">Xóa</a></p>'
                                html3+=        '</div>'
                                html3+=        '<div class="box-price">'
                                                if(i.discount){
                                html3+=                    '<span class="price pricechange">'+(i.price-(i.price*(i.discount/100))).toFixed(3)+'₫</span>'
                                                }else{
                                html3+=                    '<span class="price pricechange">'+i.price+'₫</span>'
                                                }
                                html3+=        '</div>'
                                html3+=        '<div class="quantity-block">'
                                html3+=            '<div class="input-group bootstrap-touchspin">'
                                html3+=                '<div class="input-group-btn">'
                                html3+=                    '<button onclick="downQty(\'qtyItem'+i.options.code+ '\',\'' + i.rowId + '\',' + i.id + ')"' + 'class="increase_pop btn-plus btn btn-default bootstrap-touchspin-ups" type="button">-</button>'
                                html3+=                    '<input type="text" class="form-control quantity-r2 quantity js-quantity-product input-text number-sidebar input_pop input_pop"  name="Lines" size="4" value="' + i.qty+ '">'
                                html3+=                    '<button onclick="upQty(\'qtyItem'+i.options.code+ '\',\'' + i.rowId + '\',' + i.id + ')"' + 'class="reduced_pop btn-minus btn btn-default bootstrap-touchspin-downs" type="button">+</button>'
                                html3+=                '</div>'
                                html3+=            '</div>'
                                html3+=        '</div>'
                                html3+=    '</div>'
                                html3+='</div>'
                });
                $(".count_item_pr").text(response.cartcount)
                $(".tbody-popup").html(html)
                $(".list-item-cart").html(html2)
                $(".cart-tbody").html(html3)
                $(".totals_price").text(response.total)
                $("#price _text-right").text(response.total)
                $(".total_cart").text('Thành tiền: ' + response.total+'đ')
                $(".top-subtotal").text('Thành tiền: ' + response.total+'đ')
            }
        });
})

function downQty(id,rowId,pro_id){
    var result = document.getElementById(id);
        result.value --;
        let qtyItem = result.value --;
    if( !isNaN( qtyItem ) && qtyItem > 1 ){
        var url =  "/cart/update" + '/' + rowId + '/' + qtyItem
        $.ajax({
            type: 'get',
            dataType: 'json',
            url: url,
            success:function(response){
                const cartArray = Object.keys(response.cart).map(i => response.cart[i])
                let html = "";
                let html2 = "";
                let html3 = "";
                cartArray.forEach(function(i){
                                html+='<div class="item-popup itemid-25835028">'
                                html+=    '<div style="width: 55%;" class="text-left">'
                                html+=        '<div class="item-image">'
                                html+=        '<a class="item-image" href="" title="'+i.name+'"><img alt="'+i.name+'" src="'+ i.options.img + '" width="80" /></a>'
                                html+=    '</div>'
                                html+=    '<div class="item-info">'
                                html+=        '<p class="item-name"><a href="#" title="'+i.name+'">'+i.name+'</a></p>'
                                html+=        '<p class="variant-title-popup"></p>'
                                html+=        '<p class="item-remove"><a href="javascript:;" onclick="deleteCart(\'' + i.rowId + '\')"' + 'title="Xóa"><i class="fa fa-close"></i> Bỏ sản phẩm</a></p>'
                                html+=    '</div>'
                                html+='</div>'
                                html+='<div style="width: 15%;" class="text-right">'
                                    if(i.discount){
                                html+='<div class="item-price"><span class="price pricechange">'+(i.price-(i.price*(i.discount/100))).toFixed(3)+'₫</span></div>'
                                    }else{
                                html+='<div class="item-price"><span class="price pricechange">'+i.price+'₫</span></div>'
                                    }
                                html+='</div>'
                                html+='<div style="width: 15%;" class="text-center">'
                                html+='<button onclick="downQty(\'qtyItem'+i.options.code+ '\',\'' + i.rowId + '\',' + i.id + ')"' + 'class="reduced items-counts btn-minus" type="button">–</button>'
                                html+='<input disabled="" type="text" maxlength="12" min="0"  id="qtyItem'+i.options.code+'" name="Lines" class="input-text number-sidebar" size="4" value="' + i.qty+ '">'
                                html+='<button onclick="upQty(\'qtyItem'+i.options.code+ '\',\'' + i.rowId + '\',' + i.id + ')"' + 'class="increase items-counts btn-minus" type="button">+</button>'
                                html+='</div>'
                                html+='<div style="width: 15%;" class="text-right"><span class="cart-price"> <span class="price">'+i.price*i.qty+'₫</span> </span>'
                                html+='</div>'
                                html+='</div>'
                                html2+= '<li class="item productid-25835028">'
                                html2+=    '<a class="product-image" href="" title="'+i.name+'"><img alt="'+i.name+'" src="'+ i.options.img + '" width="80"></a>'
                                html2+=    '<div class="detail-item">'
                                html2+=        '<div class="product-details">'
                                html2+=            '<a href="javascript:;"  title="Xóa" onclick="deleteCart(\'' + i.rowId + '\')"' + ' class="fa fa-remove">&nbsp;</a>'
                                html2+=            '<p class="product-name"> <a href="#" title=""'+i.name+'"">'+i.name+'</a></p>'
                                html2+=        '</div>'
                                html2+=        '<div class="product-details-bottom">'
                                                      if(i.discount){
                                html2+=                    '<span class="price pricechange">'+(i.price-(i.price*(i.discount/100))).toFixed(3)+'₫</span>'
                                                        }else{
                                html2+=                    '<span class="price pricechange">'+i.price+'₫</span>'
                                                      }
                                html2+=            '<div class="quantity-select">'
                                html2+=                '<button onclick="downQty(\'qtyItem'+i.options.code+ '\',\'' + i.rowId + '\',' + i.id + ')"' + 'class="reduced items-counts btn-minus" type="button">–</button>'
                                html2+=                '<input type="text" disabled="" maxlength="3" min="1" class="input-text number-sidebar"  name="Lines" size="4" value="' + i.qty+ '">'
                                html2+=                '<button onclick="upQty(\'qtyItem'+i.options.code+ '\',\'' + i.rowId + '\',' + i.id + ')"' + 'class="increase items-counts btn-plus" type="button">+</button>'
                                html2+=            '</div>'
                                html2+=        '</div>'
                                html2+=    '</div>'
                                html2+='</li>'
                                html3+='<div class="row shopping-cart-item productid-25835028">'
                                html3+=    '<div class="col-xs-3 img-thumnail-custom">'
                                html3+=        '<p class="image"><a href="#" title="'+i.name+'" target="_blank"><img class="img-responsive" src="'+ i.options.img + '" alt="'+i.name+'"></a></p>'
                                html3+=    '</div>'
                                html3+=    '<div class="col-right col-xs-9">'
                                html3+=        '<div class="box-info-product">'
                                html3+=            '<p class="name"><a href="#" title="'+i.name+'" target="_blank">'+i.name+'</a></p>'
                                html3+=            '<p class="action"><a href="javascript:;" onclick="deleteCart(\'' + i.rowId + '\')"' + ' class="btn btn-link btn-item-delete" title="Xóa">Xóa</a></p>'
                                html3+=        '</div>'
                                html3+=        '<div class="box-price">'
                                                if(i.discount){
                                html3+=                    '<span class="price pricechange">'+(i.price-(i.price*(i.discount/100))).toFixed(3)+'₫</span>'
                                                }else{
                                html3+=                    '<span class="price pricechange">'+i.price+'₫</span>'
                                                }
                                html3+=        '</div>'
                                html3+=        '<div class="quantity-block">'
                                html3+=            '<div class="input-group bootstrap-touchspin">'
                                html3+=                '<div class="input-group-btn">'
                                html3+=                    '<button onclick="downQty(\'qtyItem'+i.options.code+ '\',\'' + i.rowId + '\',' + i.id + ')"' + 'class="increase_pop btn-plus btn btn-default bootstrap-touchspin-ups" type="button">-</button>'
                                html3+=                    '<input type="text" class="form-control quantity-r2 quantity js-quantity-product input-text number-sidebar input_pop input_pop"  name="Lines" size="4" value="' + i.qty+ '">'
                                html3+=                    '<button onclick="upQty(\'qtyItem'+i.options.code+ '\',\'' + i.rowId + '\',' + i.id + ')"' + 'class="reduced_pop btn-minus btn btn-default bootstrap-touchspin-downs" type="button">+</button>'
                                html3+=                '</div>'
                                html3+=            '</div>'
                                html3+=        '</div>'
                                html3+=    '</div>'
                                html3+='</div>'
                });
                $(".count_item_pr").text(response.cartcount)
                $(".tbody-popup").html(html)
                $(".list-item-cart").html(html2)
                $(".cart-tbody").html(html3)
                $(".totals_price").text(response.total)
                $("#price _text-right").text(response.total)
                $(".total_cart").text('Thành tiền: ' + response.total+'đ')
                $(".top-subtotal").text('Thành tiền: ' + response.total+'đ')
                }
        });
    }else{
        return false;
    } 
}
function upQty(id,rowId,pro_id){
    var result = document.getElementById(id);
    result.value ++;
    let qtyItem = result.value ++;
    if( !isNaN( qtyItem )){
        var url =  "/cart/update" + '/' + rowId + '/' + qtyItem
        $.ajax({
            type: 'get',
            dataType: 'json',
            url: url,
            success:function(response){
                const cartArray = Object.keys(response.cart).map(i => response.cart[i])
                let html = "";
                let html2 = "";
                let html3 = "";
                cartArray.forEach(function(i){
                                html+='<div class="item-popup itemid-25835028">'
                                html+=    '<div style="width: 55%;" class="text-left">'
                                html+=        '<div class="item-image">'
                                html+=        '<a class="item-image" href="" title="'+i.name+'"><img alt="'+i.name+'" src="'+ i.options.img + '" width="80" /></a>'
                                html+=    '</div>'
                                html+=    '<div class="item-info">'
                                html+=        '<p class="item-name"><a href="#" title="'+i.name+'">'+i.name+'</a></p>'
                                html+=        '<p class="variant-title-popup"></p>'
                                html+=        '<p class="item-remove"><a href="javascript:;" onclick="deleteCart(\'' + i.rowId + '\')"' + 'title="Xóa"><i class="fa fa-close"></i> Bỏ sản phẩm</a></p>'
                                html+=    '</div>'
                                html+='</div>'
                                html+='<div style="width: 15%;" class="text-right">'
                                    if(i.discount){
                                html+='<div class="item-price"><span class="price pricechange">'+(i.price-(i.price*(i.discount/100))).toFixed(3)+'₫</span></div>'
                                    }else{
                                html+='<div class="item-price"><span class="price pricechange">'+i.price+'₫</span></div>'
                                    }
                                html+='</div>'
                                html+='<div style="width: 15%;" class="text-center">'
                                html+='<button onclick="downQty(\'qtyItem'+i.options.code+ '\',\'' + i.rowId + '\',' + i.id + ')"' + 'class="reduced items-counts btn-minus" type="button">–</button>'
                                html+='<input disabled="" type="text" maxlength="12" min="0"  id="qtyItem'+i.options.code+'" name="Lines" class="input-text number-sidebar" size="4" value="' + i.qty+ '">'
                                html+='<button onclick="upQty(\'qtyItem'+i.options.code+ '\',\'' + i.rowId + '\',' + i.id + ')"' + 'class="increase items-counts btn-minus" type="button">+</button>'
                                html+='</div>'
                                html+='<div style="width: 15%;" class="text-right"><span class="cart-price"> <span class="price">'+i.price*i.qty+'₫</span> </span>'
                                html+='</div>'
                                html+='</div>'
                                html2+= '<li class="item productid-25835028">'
                                html2+=    '<a class="product-image" href="" title="'+i.name+'"><img alt="'+i.name+'" src="'+ i.options.img + '" width="80"></a>'
                                html2+=    '<div class="detail-item">'
                                html2+=        '<div class="product-details">'
                                html2+=            '<a href="javascript:;"  title="Xóa" onclick="deleteCart(\'' + i.rowId + '\')"' + ' class="fa fa-remove">&nbsp;</a>'
                                html2+=            '<p class="product-name"> <a href="#" title=""'+i.name+'"">'+i.name+'</a></p>'
                                html2+=        '</div>'
                                html2+=        '<div class="product-details-bottom">'
                                                      if(i.discount){
                                html2+=                    '<span class="price pricechange">'+(i.price-(i.price*(i.discount/100))).toFixed(3)+'₫</span>'
                                                        }else{
                                html2+=                    '<span class="price pricechange">'+i.price+'₫</span>'
                                                      }
                                html2+=            '<div class="quantity-select">'
                                html2+=                '<button onclick="downQty(\'qtyItem'+i.options.code+ '\',\'' + i.rowId + '\',' + i.id + ')"' + 'class="reduced items-counts btn-minus" type="button">–</button>'
                                html2+=                '<input type="text" disabled="" maxlength="3" min="1" class="input-text number-sidebar"  name="Lines" size="4" value="' + i.qty+ '">'
                                html2+=                '<button onclick="upQty(\'qtyItem'+i.options.code+ '\',\'' + i.rowId + '\',' + i.id + ')"' + 'class="increase items-counts btn-plus" type="button">+</button>'
                                html2+=            '</div>'
                                html2+=        '</div>'
                                html2+=    '</div>'
                                html2+='</li>'
                                html3+='<div class="row shopping-cart-item productid-25835028">'
                                html3+=    '<div class="col-xs-3 img-thumnail-custom">'
                                html3+=        '<p class="image"><a href="#" title="'+i.name+'" target="_blank"><img class="img-responsive" src="'+ i.options.img + '" alt="'+i.name+'"></a></p>'
                                html3+=    '</div>'
                                html3+=    '<div class="col-right col-xs-9">'
                                html3+=        '<div class="box-info-product">'
                                html3+=            '<p class="name"><a href="#" title="'+i.name+'" target="_blank">'+i.name+'</a></p>'
                                html3+=            '<p class="action"><a href="javascript:;" onclick="deleteCart(\'' + i.rowId + '\')"' + ' class="btn btn-link btn-item-delete" title="Xóa">Xóa</a></p>'
                                html3+=        '</div>'
                                html3+=        '<div class="box-price">'
                                                if(i.discount){
                                html3+=                    '<span class="price pricechange">'+(i.price-(i.price*(i.discount/100))).toFixed(3)+'₫</span>'
                                                }else{
                                html3+=                    '<span class="price pricechange">'+i.price+'₫</span>'
                                                }
                                html3+=        '</div>'
                                html3+=        '<div class="quantity-block">'
                                html3+=            '<div class="input-group bootstrap-touchspin">'
                                html3+=                '<div class="input-group-btn">'
                                html3+=                    '<button onclick="downQty(\'qtyItem'+i.options.code+ '\',\'' + i.rowId + '\',' + i.id + ')"' + 'class="increase_pop btn-plus btn btn-default bootstrap-touchspin-ups" type="button">-</button>'
                                html3+=                    '<input type="text" class="form-control quantity-r2 quantity js-quantity-product input-text number-sidebar input_pop input_pop"  name="Lines" size="4" value="' + i.qty+ '">'
                                html3+=                    '<button onclick="upQty(\'qtyItem'+i.options.code+ '\',\'' + i.rowId + '\',' + i.id + ')"' + 'class="reduced_pop btn-minus btn btn-default bootstrap-touchspin-downs" type="button">+</button>'
                                html3+=                '</div>'
                                html3+=            '</div>'
                                html3+=        '</div>'
                                html3+=    '</div>'
                                html3+='</div>'
                });
                $(".count_item_pr").text(response.cartcount)
                $(".tbody-popup").html(html)
                $(".list-item-cart").html(html2)
                $(".cart-tbody").html(html3)
                $(".totals_price").text(response.total)
                $("#price _text-right").text(response.total)
                $(".total_cart").text('Thành tiền: ' + response.total+'đ')
                $(".top-subtotal").text('Thành tiền: ' + response.total+'đ')
                }
        });
    }else{
        return false;
    } 
}
function deleteCart(rowId){
    var url =  "/cart/delete" + '/' + rowId 
        $.ajax({
            type: 'get',
            dataType: 'json',
            url: url,
            success:function(response){
                const cartArray = Object.keys(response.cart).map(i => response.cart[i])
                let html = "";
                let html2 = "";
                let html3 = "";
                cartArray.forEach(function(i){
                                html+='<div class="item-popup itemid-25835028">'
                                html+=    '<div style="width: 55%;" class="text-left">'
                                html+=        '<div class="item-image">'
                                html+=        '<a class="item-image" href="" title="'+i.name+'"><img alt="'+i.name+'" src="'+ i.options.img + '" width="80" /></a>'
                                html+=    '</div>'
                                html+=    '<div class="item-info">'
                                html+=        '<p class="item-name"><a href="#" title="'+i.name+'">'+i.name+'</a></p>'
                                html+=        '<p class="variant-title-popup"></p>'
                                html+=        '<p class="item-remove"><a href="javascript:;" onclick="deleteCart(\'' + i.rowId + '\')"' + 'title="Xóa"><i class="fa fa-close"></i> Bỏ sản phẩm</a></p>'
                                html+=    '</div>'
                                html+='</div>'
                                html+='<div style="width: 15%;" class="text-right">'
                                    if(i.discount){
                                html+='<div class="item-price"><span class="price pricechange">'+(i.price-(i.price*(i.discount/100))).toFixed(3)+'₫</span></div>'
                                    }else{
                                html+='<div class="item-price"><span class="price pricechange">'+i.price+'₫</span></div>'
                                    }
                                html+='</div>'
                                html+='<div style="width: 15%;" class="text-center">'
                                html+='<button onclick="downQty(\'qtyItem'+i.options.code+ '\',\'' + i.rowId + '\',' + i.id + ')"' + 'class="reduced items-counts btn-minus" type="button">–</button>'
                                html+='<input disabled="" type="text" maxlength="12" min="0"  id="qtyItem'+i.options.code+'" name="Lines" class="input-text number-sidebar" size="4" value="' + i.qty+ '">'
                                html+='<button onclick="upQty(\'qtyItem'+i.options.code+ '\',\'' + i.rowId + '\',' + i.id + ')"' + 'class="increase items-counts btn-minus" type="button">+</button>'
                                html+='</div>'
                                html+='<div style="width: 15%;" class="text-right"><span class="cart-price"> <span class="price">'+i.price*i.qty+'₫</span> </span>'
                                html+='</div>'
                                html+='</div>'
                                html2+= '<li class="item productid-25835028">'
                                html2+=    '<a class="product-image" href="" title="'+i.name+'"><img alt="'+i.name+'" src="'+ i.options.img + '" width="80"></a>'
                                html2+=    '<div class="detail-item">'
                                html2+=        '<div class="product-details">'
                                html2+=            '<a href="javascript:;"  title="Xóa" onclick="deleteCart(\'' + i.rowId + '\')"' + ' class="fa fa-remove">&nbsp;</a>'
                                html2+=            '<p class="product-name"> <a href="#" title=""'+i.name+'"">'+i.name+'</a></p>'
                                html2+=        '</div>'
                                html2+=        '<div class="product-details-bottom">'
                                                      if(i.discount){
                                html2+=                    '<span class="price pricechange">'+(i.price-(i.price*(i.discount/100))).toFixed(3)+'₫</span>'
                                                        }else{
                                html2+=                    '<span class="price pricechange">'+i.price+'₫</span>'
                                                      }
                                html2+=            '<div class="quantity-select">'
                                html2+=                '<button onclick="downQty(\'qtyItem'+i.options.code+ '\',\'' + i.rowId + '\',' + i.id + ')"' + 'class="reduced items-counts btn-minus" type="button">–</button>'
                                html2+=                '<input type="text" disabled="" maxlength="3" min="1" class="input-text number-sidebar"  name="Lines" size="4" value="' + i.qty+ '">'
                                html2+=                '<button onclick="upQty(\'qtyItem'+i.options.code+ '\',\'' + i.rowId + '\',' + i.id + ')"' + 'class="increase items-counts btn-plus" type="button">+</button>'
                                html2+=            '</div>'
                                html2+=        '</div>'
                                html2+=    '</div>'
                                html2+='</li>'
                                html3+='<div class="row shopping-cart-item productid-25835028">'
                                html3+=    '<div class="col-xs-3 img-thumnail-custom">'
                                html3+=        '<p class="image"><a href="#" title="'+i.name+'" target="_blank"><img class="img-responsive" src="'+ i.options.img + '" alt="'+i.name+'"></a></p>'
                                html3+=    '</div>'
                                html3+=    '<div class="col-right col-xs-9">'
                                html3+=        '<div class="box-info-product">'
                                html3+=            '<p class="name"><a href="#" title="'+i.name+'" target="_blank">'+i.name+'</a></p>'
                                html3+=            '<p class="action"><a href="javascript:;" onclick="deleteCart(\'' + i.rowId + '\')"' + ' class="btn btn-link btn-item-delete" title="Xóa">Xóa</a></p>'
                                html3+=        '</div>'
                                html3+=        '<div class="box-price">'
                                                if(i.discount){
                                html3+=                    '<span class="price pricechange">'+(i.price-(i.price*(i.discount/100))).toFixed(3)+'₫</span>'
                                                }else{
                                html3+=                    '<span class="price pricechange">'+i.price+'₫</span>'
                                                }
                                html3+=        '</div>'
                                html3+=        '<div class="quantity-block">'
                                html3+=            '<div class="input-group bootstrap-touchspin">'
                                html3+=                '<div class="input-group-btn">'
                                html3+=                    '<button onclick="downQty(\'qtyItem'+i.options.code+ '\',\'' + i.rowId + '\',' + i.id + ')"' + 'class="increase_pop btn-plus btn btn-default bootstrap-touchspin-ups" type="button">-</button>'
                                html3+=                    '<input type="text" class="form-control quantity-r2 quantity js-quantity-product input-text number-sidebar input_pop input_pop"  name="Lines" size="4" value="' + i.qty+ '">'
                                html3+=                    '<button onclick="upQty(\'qtyItem'+i.options.code+ '\',\'' + i.rowId + '\',' + i.id + ')"' + 'class="reduced_pop btn-minus btn btn-default bootstrap-touchspin-downs" type="button">+</button>'
                                html3+=                '</div>'
                                html3+=            '</div>'
                                html3+=        '</div>'
                                html3+=    '</div>'
                                html3+='</div>'
                });
                $(".count_item_pr").text(response.cartcount)
                $(".tbody-popup").html(html)
                $(".list-item-cart").html(html2)
                $(".cart-tbody").html(html3)
                $(".totals_price").text(response.total)
                $("#price _text-right").text(response.total)
                $(".total_cart").text('Thành tiền: ' + response.total+'đ')
                $(".top-subtotal").text('Thành tiền: ' + response.total+'đ')
                }
        });
}