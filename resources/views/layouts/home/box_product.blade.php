<section class="awe-section-4">
    <div class="section_san_pham section_san_pham_moi">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="new_title">
                        <h4>{{getLanguage('featured_products')}}</h4>
                        <h2><a href="san-pham-moi" title="Hàng mới về">{{getLanguage('new_products')}}</a></h2>
                    </div>
                </div>
                <div class="ol-md-12 col-sm-12 col-xs-12">
                    <div class="evo-block-san-pham">
                        <div class="row">
                            <div class="col-md-9 col-md-push-3">
                                <div class="evo-block-product evo-slick">
                                    @foreach ($box_product as $item)
                                    <div class="evo-item">
                                        @foreach ($item as $i)
                                        <div class="evo-product-block-item ">
                                                <div class="product-img">
                                                    @if($i['discount'])
                                                    <div class="product-sale" >
                                                    <span>{{$i['discount']}}%</span>
                                                    </div>
                                                    @endif
                                                    <a href="{{ route('product.detail', ['slug' => $i['slug']]) }}"
                                                        title="{{$i['name']}}"
                                                        class="image-resize">
                                                        <img class="lazy"
                                                        src="{{json_decode($i['images'])[0]}}"
                                                            data-src="{{json_decode($i['images'])[0]}}"
                                                            alt="{{$i['name']}}" />
                                                    </a>
                                                    <div class="button-add">
                                                        <div 
                                                            class="hidden-md variants form-nut-grid"
                                                            >
                                                            {{-- <a href="{{ route('cart.add', ['id' => $i['id']]) }}"
                                                                title="Mua ngay"  class="action">Mua ngay</a> --}}
                                                            <button type="button" title="Mua ngay">
                                                                <a style="color:#fff" href="{{route('cart.now',$i['id'])}}"
                                                                title="Mua ngay">{{getLanguage('buy_now')}}</a>
                                                            </button>
                                                            <button class=""
                                                                data-toggle="modal" data-target="#popup-cart" data-id="{{$i['id']}}">{{getLanguage('add_to_basket')}}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="product-detail clearfix">
                                                    <div class="box-pro-detail">
                                                        <h3 class="pro-name">
                                                            <a href="{{ route('product.detail', ['slug' => $i['slug']]) }}"
                                                                title="{{$i['name']}}">{{$i['name']}}</a>
                                                        </h3>
                                                        <div class="box-pro-prices">
                                                            @if($i['discount'])
                                                                <p class="pro-price">
                                                                {{number_format($i['price']-($i['price']*($i['discount']/100)))}}₫
                                                                    <span class="pro-price-del">
                                                                        <del class="compare-price">{{number_format($i['price'])}}</del>
                                                                    </span>
                                                                </p>
                                                            @else
                                                                <p class="pro-price">
                                                                    {{number_format($i['price'])}}₫
                                                                </p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                        @endforeach
                                    </div>
                                    @endforeach
                                    
                                </div>
                            </div>
                            <div class="col-md-3 col-md-pull-9">
                                <div class="banner clearfix">
                                    <a class="single_image_effect" href="#" title="RIBETO">
                                        <img src="{{url('frontend/images/sp1488x640.jpg')}}"
                                            data-src="{{url('frontend/images/sp1488x640.jpg')}}"
                                            alt="RIBETO" class="lazy img-responsive center-block" />
                                    </a>
                                    <a class="single_image_effect" href="#" title="RIBETO">
                                        <img src="{{url('frontend/images/sp2488x640.jpg')}}"
                                            data-src="{{url('frontend/images/sp2488x640.jpg')}}"
                                            alt="RIBETO" class="lazy img-responsive center-block" />
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>