<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace'=>'Api','middleware' => 'api'],function(){
	Route::post('login','AuthController@login');
	Route::post('upload-image','AllController@uploadImage');
	Route::post('upload-image-multi','AllController@uploadImageMulti');
	
});
Route::group(['namespace'=>'Api','middleware'=>'auth:api'],function(){

	Route::get('/rolee', function () {
		$user = request()->user();
		dd($user->can('view-users'));
		return view('welcome');
	})->middleware('auth');

	Route::post('logout','AuthController@logout'); 
	Route::get('getNotification','NotificationController@get');
	Route::get('profile','AuthController@authentication');
	Route::group(['prefix'=>'menu'],function(){
		Route::get('listMenu','MenuController@listMenu');
		Route::get('getAllMenu','MenuController@getAllMenu');
		Route::post('saveChangeMenu','MenuController@saveChangemenu');
		Route::post('addNewMenu','MenuController@addNewMenu');
		Route::get('getEditMenu/{id}','MenuController@getEditMenu');
		Route::post('saveEditMenuById/{id}','MenuController@saveEditMenuById');
		Route::get('deleteMenuById/{id}','MenuController@deleteMenuById');
	});
	Route::group(['prefix' => 'language'], function () {
		Route::post('detailLanguage', 'LanguageController@detailLanguage')->name('language.detail');
		Route::post('saveLanguage', 'LanguageController@saveLanguage')->name('language.save');
		Route::post('searchLanguage', 'LanguageController@searchLanguage')->name('language.search');
		Route::post('activeLanguage', 'LanguageController@getActiveLanguage')->name('language.active');
		Route::post('saveLanguageStatic', 'LanguageController@saveLanguageStatic')->name('language.saveLanguageStatic');
		Route::post('searchLanguageStatic', 'LanguageController@searchLanguageStatic')->name('language.searchLanguageStatic');
		Route::post('saveLanguageStaticByLang', 'LanguageController@saveLanguageStaticByLang')->name('language.saveLanguageStaticByLang');
		Route::post('deleteLanguage', 'LanguageController@deleteLanguage')->name('language.delete');
	}); 
	Route::group(['prefix'=>'pagecontent'], function(){
		Route::post('add','PageContentController@add');
	});
	Route::group(['prefix'=>'product', 'namespace'=>'Product'], function(){
		Route::post('create','ProductController@create');
		Route::post('list','ProductController@list');
		Route::get('edit/{code}/{language}','ProductController@edit');
		Route::get('delete/{id}','ProductController@delete');
		Route::group(['prefix'=>'category'], function(){
			Route::post('add','CategoryController@add');
			Route::post('list','CategoryController@list');
			Route::get('delete/{id}','CategoryController@delete');
			Route::get('edit/{quiz_id}/{language}','CategoryController@edit');
		});
	});
	Route::group(['prefix'=>'blog', 'namespace'=>'Blog'], function(){
		Route::post('create','BlogController@create');
		Route::post('list','BlogController@list');
		Route::get('edit/{code}/{language}','BlogController@edit');
		Route::get('delete/{quiz_id}','BlogController@delete');
		Route::group(['prefix'=>'category'], function(){
			Route::post('add','BlogCategoryController@add');
			Route::post('list','BlogCategoryController@list');
			Route::get('edit/{quiz_id}/{language}','BlogCategoryController@edit');
		});
	});
	Route::group(['prefix'=>'page_content'], function(){
		Route::post('create','PageContentController@create');
		Route::post('list','PageContentController@list');
		Route::get('edit/{quiz_id}/{language}','PageContentController@edit');
	});
	Route::group(['prefix'=>'website','namespace'=>'Website'], function(){
		Route::post('banner','BannerController@createOrUpdate');
		Route::get('list-banner','BannerController@list');
		Route::post('partner','PartnerController@createOrUpdate');
		Route::post('prize','PartnerController@createOrUpdatePrize');
		Route::get('list-partner','PartnerController@listPartner');
		Route::get('list-prize','PartnerController@listPrize');
		Route::get('setting','SettingController@setting');
		Route::post('save-setting','SettingController@postsetting');
	});
	Route::group(['prefix'=>'customer','namespace'=>'Customer'], function(){
		Route::post('list','CustomerController@list');
		Route::post('add','CustomerController@create');
		Route::get('edit/{id}','CustomerController@getEdit');
		Route::post('active','CustomerController@activeCustomer');
		Route::post('changeStatus','CustomerController@changeStatus');
		Route::post('edit-profile','CustomerController@postEdit');
	});
	Route::group(['prefix'=>'bill','namespace'=>'Bill'], function(){
		Route::post('add','BillController@add');
		Route::post('draft','BillController@draft');
		Route::get('detail/{code}','BillController@detail');
		Route::post('change-status','BillController@changeStatus');
	});
	Route::group(['prefix'=>'home'], function(){
		Route::post('chart','HomeController@chart');
	});
});
