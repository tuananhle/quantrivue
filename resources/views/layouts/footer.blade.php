<footer class="footer bg-footer">
    <div class="block_newsletter">
        <div class="container">
            <div class="row">
                <div class="title-block col-md-6">
                    <div class="news-icon"><i class="fa fa-envelope-o"></i></div>
                    <div class="title-text">
                        <p class="h3">{{getLanguage('sign_up_to_receive_news')}}</p>
                        <p>{{getLanguage('get_great_deal_from')}}</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <form
                        action="//facebook.us7.list-manage.com/subscribe/post?u=97ba6d3ba28239250923925a8&id=4ef3a755a8"
                        method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form"
                        target="_blank">
                        <div class="input-group">
                            <input type="email" class="form-control" value="" placeholder="Email của bạn"
                                name="EMAIL" id="mail">
                            <span class="input-group-btn">
                                <button class="btn btn-default" name="subscribe" id="subscribe" type="submit">{{getLanguage('register')}}</button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="site-footer">
        <div class="container">
            <div class="footer-inner padding-bottom-20">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-3 fix-clear">
                        <div class="footer-widget footer-has-logo">
                            <h3>
                                <a href="/" title="RIBETO">
                                    <img style="margin-top: -7%;" src="{{$setting->logo}}"
                                        data-src="{{$setting->logo}}"
                                        alt="RIBETO" class="lazy img-responsive center-block" />
                                </a>
                            </h3>
                            <div class="footer-description">
                                {{$setting->company}}
                            </div>
                            <ul class="footer-social">
                                <li class="twitter">
                                    <a href="#" title="Theo dõi Twitter RIBETO" target="_blank"><i
                                            class="fa fa-twitter" aria-hidden="true"></i></a>
                                </li>
                                <li class="facebook">
                                    <a href="#" title="Theo dõi Facebook RIBETO" target="_blank"><i
                                            class="fa fa-facebook" aria-hidden="true"></i></a>
                                </li>
                                <li class="instagram">
                                    <a href="#" title="Theo dõi Instagram RIBETO" target="_blank"><i
                                            class="fa fa-instagram" aria-hidden="true"></i></a>
                                </li>
                                <li class="youtube">
                                    <a href="#" title="Theo dõi Youtube RIBETO" target="_blank"><i
                                            class="fa fa-youtube" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3 fix-clear">
                        <div class="footer-widget footer-contact">
                            <h3>{{getLanguage('contact_info')}}</h3>
                            <ul class="list-menu">
                                <li><i class="fa fa-map-marker" aria-hidden="true"></i> {{$setting->address1}}</li>
                                <li><i class="fa fa-phone" aria-hidden="true"></i> <a href="{{$setting->phone1}}"
                                        title="{{$setting->phone1}}">{{$setting->phone1}}</a></li>
                                <li><i class="fa fa-envelope" aria-hidden="true"></i> <a
                                        href="mailto:{{$setting->email}}"
                                        title="{{$setting->email}}">{{$setting->email}}</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-3 fix-clear">
                        <div class="footer-widget had-click">
                            <h3>Menu nhanh</h3>
                            <ul class="list-menu has-click">
                                <li><a href="/" title="Trang chủ" rel="nofollow">{{getLanguage('Trangchu')}}</a></li>
                                <li><a href="{{ route('product.list') }}" title="Giới thiệu" rel="nofollow">{{getLanguage('product')}}</a></li>
                                <li><a href="{{ route('blog.list') }}" title="Sản phẩm" rel="nofollow">{{getLanguage('news')}}</a></li>
                                @foreach($pagecontent as $item)
                                <li><a href="{{route('pagecontent.detail',['slug'=>$item->slug])}}" title="{{$item->title}}" rel="nofollow">{{$item->title}}</a>
                                </li>
                                @endforeach
                                <li><a href="/fukujyusen" title="Fukujyusen" rel="nofollow">Fukujyusen</a></li>
                                <li><a href="{{route('contact.index')}}" title="Mẹo hay" rel="nofollow">{{getLanguage('contact')}}</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-3 fix-clear">
                        <div class="footer-widget had-click">
                            <h3>Dịch vụ nổi bật</h3>
                            <ul class="list-menu has-click">
                                <li><a href="/" title="Trang chủ" rel="nofollow">{{getLanguage('Trangchu')}}</a></li>
                                <li><a href="{{ route('product.list') }}" title="Giới thiệu" rel="nofollow">{{getLanguage('product')}}</a></li>
                                <li><a href="{{ route('blog.list') }}" title="Sản phẩm" rel="nofollow">{{getLanguage('news')}}</a></li>
                                @foreach($pagecontent as $item)
                                <li><a href="{{route('pagecontent.detail',['slug'=>$item->slug])}}" title="{{$item->title}}" rel="nofollow">{{$item->title}}</a>
                                </li>
                                @endforeach
                                <li><a href="/fukujyusen" title="Fukujyusen" rel="nofollow">Fukujyusen</a></li>
                                <li><a href="{{route('contact.index')}}" title="Mẹo hay" rel="nofollow">{{getLanguage('contact')}}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright clearfix">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <span>© Bản quyền thuộc về <b>RIBETO</b> <span class="s480-f">
                </div>
            </div>
            <div class="back-to-top" title="Lên đầu trang"><i class="fa fa-angle-up" aria-hidden="true"></i></div>
        </div>
    </div>
</footer>