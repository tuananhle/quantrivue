<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\product\Category;
use App\models\product\Product;
use Session;
use App\models\website\Banner;
use App\models\website\Partner;
use App\models\website\Prize;
use App\models\blog\Blog;
use Cart;

class HomeController extends Controller
{
    public function home()
    {
        if(Session::get('locale') == null){
            Session::put('locale', app()->getLocale());
        }
        $language_current = Session::get('locale');
        $data['banner'] = Banner::where('status' ,  1)->get();
        $product = Product::where(['language'=>$language_current,'status' => 1])->get()->toArray();
        $data['box_product'] = array_chunk($product, 2);
        $data['cart'] = Cart::content();
        $data['partner'] =  Partner::where(['status' => 1])->get();
        $data['prize'] =  Prize::where(['status' => 1])->get();
        $data['blogs'] =  Blog::where(['language'=>$language_current,'status' => 1])->get();
        return view('home',$data);
    }
}
