<section class="awe-section-10">
    <div class="container section_blogs">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="new_title">
                    <h2><a href="tin-tuc" title="Vào bếp">{{getLanguage('news')}}</a></h2>
                    <p>{{getLanguage('update_news_from_us')}}</p>
                </div>
                <div class="evo-owl-blog evo-slick">
                    @foreach ($blogs as $item)
                    <div class="news-items">
                        <a href="{{route('blog.detail',['slug'=>$item->slug])}}"
                    title="{{$item->title}}"
                            class="clearfix evo-item-blogs">
                            <div class="evo-article-image">
                                <img src="{{$item->image}}"
                                    data-src="{{$item->image}}"
                                    alt="{{$item->image}}"
                                    class="lazy img-responsive center-block" />
                            </div>
                            <h3 class="line-clamps">{{$item->title}}</h3>
                            <p>{!!$item->description!!}</p>
                        </a>
                    </div>
                    @endforeach
                    
                </div>
            </div>
        </div>
    </div>
</section>