<section class="awe-section-1">
    <div class="home-slider owl-carousel pb-4" data-lg-items='1' data-md-items='1' data-sm-items='1'
        data-xs-items="1" data-margin='0' data-nav="true">
        @foreach($banner as $item)
        <div class="item manh">
            <a href="javascript:;" title="alt slider demo">
                <picture>
                    <source media="(max-width: 767px)"
                        srcset="{{$item->image}}">
                    <img src="{{$item->image}}" alt="alt slider demo" width="100%" />
                </picture>
            </a>
        </div>
        @endforeach
    </div>
</section>