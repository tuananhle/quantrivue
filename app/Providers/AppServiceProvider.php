<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Session,View;
use App\models\product\Product;
use App\models\website\Banner;
use App\models\website\Setting;
use Cart,Auth;
use App\models\PageContent;
use Laravel\Dusk\DuskServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) 
        {
            if(Auth::guard('customer')->user() != null){
                $profile = Auth::guard('customer')->user();
            }else{
                $profile = "";
            }
            $language_current = Session::get('locale');
            $totalcart = Cart::total();
            $setting = Setting::first();
            $cartcount = Cart::count();
            $cart_content = Cart::content();
            $product_menu = Product::where(['language'=>$language_current,'status'=> 1 ])->get();
            $pagecontent = PageContent::where(['language'=>$language_current,'status'=> 1 ])->get();
            $view->with([
                'product_menu' => $product_menu,
                'totalcart' => $totalcart,
                'cartcounts' => $cartcount,
                'cart_content' => $cart_content,
                'pagecontent' => $pagecontent,
                'profile' => $profile,
                'setting' => $setting,
                ]);    
        });  
    }
}
