@extends('layouts.master')
@section('css')
<link href="{{asset('frontend/asset/css/evo-carts.scss.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('title') Giỏ hàng @endsection
@section('description') Giỏ hàng của bạn @endsection 
@section('url') {{ route('cart.list') }} @endsection
@section('image') {{ route('contact.index') }} @endsection

@section('content')
<section class="bread-crumb margin-bottom-10">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">					
                    <li class="home"><a itemprop="url" href="/" title="Trang chủ"><span itemprop="title">Trang chủ</span></a><span><i class="fa fa-angle-right"></i></span></li>
                    
                    <li><strong itemprop="title">Giỏ hàng</strong></li>
                    
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="container white collections-container margin-bottom-20 margin-top-30">
        <div class="white-background">
           <div class="row">
              <div class="col-md-12">
                 <div class="shopping-cart">
                    <div class="visible-md visible-lg">
                       <div class="shopping-cart-table">
                          <div class="row">
                             <div class="col-md-12">
                                <h1 class="lbl-shopping-cart lbl-shopping-cart-gio-hang">Giỏ hàng <span>(<span class="count_item_pr">8</span> sản phẩm)</span></h1>
                             </div>
                          </div>
                          <div class="row">
                             <div class="col-main cart_desktop_page cart-page">
                                <form id="shopping-cart" action="/cart" method="post" novalidate="" class="has-validation-callback">
                                   <div class="cart page_cart cart_des_page hidden-xs-down">
                                      <div class="col-xs-9 cart-col-1">
                                         <div class="cart-tbody">
                                                @foreach ($cart as $item)
                                                    <div class="row shopping-cart-item productid-25835028">
                                                        <div class="col-xs-3 img-thumnail-custom">
                                                            <p class="image"><a href="{{ route('product.detail', ['slug' => $item->options->slug]) }}" title="{{$item->name}}" target="_blank"><img class="img-responsive" src="{{$item->options->img ? $item->options->img : '' }}" alt="{{$item->name}}"></a></p>
                                                        </div>
                                                        <div class="col-right col-xs-9">
                                                            <div class="box-info-product">
                                                                <p class="name"><a href="{{ route('product.detail', ['slug' => $item->options->slug]) }}" title="{{$item->name}}" target="_blank">{{$item->name}}</a></p>
                                                                <p class="action"><a href="javascript:;" class="btn btn-link btn-item-delete"  title="Xóa">Xóa</a></p>
                                                            </div>
                                                            <div class="box-price">
                                                                    @if($item->discount)
                                                                        <span class="price pricechange">{{number_format($item->price-($item->price*($item->discount/100)))}}₫</span>
                                                                    @else
                                                                        <span class="price pricechange">{{number_format($item->price)}}₫</span>
                                                                    @endif
                                                            </div>
                                                            <div class="quantity-block">
                                                                <div class="input-group bootstrap-touchspin">
                                                                    <div class="input-group-btn">
                                                                        <button onclick="downQty('qtyItem{{$item->options->code}}','{{$item->rowId}}',{{$item->id}})" class="increase_pop btn-plus btn btn-default bootstrap-touchspin-ups" type="button">-</button>
                                                                        <input type="text" class="form-control quantity-r2 quantity js-quantity-product input-text number-sidebar input_pop input_pop" id="qtyItem{{$item->options->code}}" name="Lines" size="4" value="{{$item->qty}}">
                                                                        <button onclick="upQty('qtyItem{{$item->options->code}}','{{$item->rowId}}',{{$item->id}})" class="reduced_pop btn-minus btn btn-default bootstrap-touchspin-downs" type="button">+</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            {{-- @else 
                                            <div class="row shopping-cart-item productid-25835028">
                                                <p>Chưa có sản phầm nào trong giỏ hàng</p>
                                            </div>
                                            @endif --}}
                                         </div>
                                      </div>
                                      <div class="col-xs-3 cart-col-2 cart-collaterals cart_submit">
                                         <div id="right-affix">
                                            <div class="each-row">
                                               <div class="box-style fee">
                                                  <p class="list-info-price"><span>Tạm tính: </span><strong class="totals_price price _text-right text_color_right1" id="price _text-right">{{$totalcart}}₫</strong></p>
                                               </div>
                                               <div class="box-style fee">
                                                  <div class="total2 clearfix">
                                                     <span class="text-label">Thành tiền: </span>
                                                     <div class="amount">
                                                        <p><strong class="totals_price">{{$totalcart}}₫</strong></p>
                                                     </div>
                                                  </div>
                                               </div>
                                               <button class="button btn btn-large btn-block btn-danger btn-checkout evo-button" title="Thanh toán ngay" type="button" onclick="window.location.href='{{route('pay.index')}}'">Thanh toán ngay</button>
                                               <button class="button btn-proceed-checkout btn btn-large btn-block btn-danger btn-checkouts" title="Tiếp tục mua hàng" type="button" onclick="window.location.href='/san-pham'">Tiếp tục mua hàng</button>
                                            </div>
                                         </div>
                                      </div>
                                   </div>
                                </form>
                             </div>
                          </div>
                       </div>
                    </div>
                    <div class="visible-sm visible-xs">
                       <div class="cart-mobile">
                          <form action="/cart" method="post" class="margin-bottom-0 has-validation-callback">
                             <div class="header-cart">
                                <div class="title-cart clearfix">
                                   <h3>Giỏ hàng của bạn</h3>
                                </div>
                             </div>
                             <div class="header-cart-content">
                                <div class="cart_page_mobile content-product-list">
                                   <div class="item-product item productid-25835028 ">
                                      <div class="item-product-cart-mobile"><a class="product-images1" href="/nuoc-giai-khat-co-gas-pepsi-loc-6-chai-x-390ml" title="Nước giải khát có gas Pepsi lốc 6 chai x 390ml"><img width="80" height="150" alt="" src="//bizweb.dktcdn.net/thumb/small/100/360/151/products/pepsi.jpg"></a></div>
                                      <div class="title-product-cart-mobile">
                                         <h3><a href="/nuoc-giai-khat-co-gas-pepsi-loc-6-chai-x-390ml" title="Nước giải khát có gas Pepsi lốc 6 chai x 390ml">Nước giải khát có gas Pepsi lốc 6 chai x 390ml</a></h3>
                                         <p>Giá: <span class="pricechange">37.800₫</span></p>
                                      </div>
                                      <div class="select-item-qty-mobile">
                                         <div class="txt_center"><input class="variantID" type="hidden" name="variantId" value="25835028"><button onclick="var result = document.getElementById('qtyMobile25835028'); var qtyMobile25835028 = result.value; if( !isNaN( qtyMobile25835028 ) &amp;&amp; qtyMobile25835028 > 1 ) result.value--;return false;" class="reduced items-count btn-minus" type="button">–</button><input type="text" maxlength="12" min="0" class="input-text number-sidebar qtyMobile25835028" id="qtyMobile25835028" name="Lines" size="4" value="6"><button onclick="var result = document.getElementById('qtyMobile25835028'); var qtyMobile25835028 = result.value; if( !isNaN( qtyMobile25835028 )) result.value++;return false;" class="increase items-count btn-plus" type="button">+</button></div>
                                         <a class="button remove-item remove-item-cart" href="javascript:;" data-id="25835028" title="Xóa">Xoá</a>
                                      </div>
                                   </div>
                                   <div class="item-product item productid-25835031 ">
                                      <div class="item-product-cart-mobile"><a class="product-images1" href="/nuoc-dua-xiem-huong-vi-sen-cocoxim-hop-330ml" title="Nước dừa xiêm hương vị sen Cocoxim hộp 330ml"><img width="80" height="150" alt="" src="//bizweb.dktcdn.net/thumb/small/100/360/151/products/nuocdua.jpg"></a></div>
                                      <div class="title-product-cart-mobile">
                                         <h3><a href="/nuoc-dua-xiem-huong-vi-sen-cocoxim-hop-330ml" title="Nước dừa xiêm hương vị sen Cocoxim hộp 330ml">Nước dừa xiêm hương vị sen Cocoxim hộp 330ml</a></h3>
                                         <p>Giá: <span class="pricechange">13.500₫</span></p>
                                      </div>
                                      <div class="select-item-qty-mobile">
                                         <div class="txt_center"><input class="variantID" type="hidden" name="variantId" value="25835031"><button onclick="var result = document.getElementById('qtyMobile25835031'); var qtyMobile25835031 = result.value; if( !isNaN( qtyMobile25835031 ) &amp;&amp; qtyMobile25835031 > 1 ) result.value--;return false;" class="reduced items-count btn-minus" type="button" disabled="">–</button><input type="text" maxlength="12" min="0" class="input-text number-sidebar qtyMobile25835031" id="qtyMobile25835031" name="Lines" size="4" value="1"><button onclick="var result = document.getElementById('qtyMobile25835031'); var qtyMobile25835031 = result.value; if( !isNaN( qtyMobile25835031 )) result.value++;return false;" class="increase items-count btn-plus" type="button">+</button></div>
                                         <a class="button remove-item remove-item-cart" href="javascript:;" data-id="25835031" title="Xóa">Xoá</a>
                                      </div>
                                   </div>
                                </div>
                                <div class="header-cart-price" style="">
                                   <div class="title-cart clearfix">
                                      <h3 class="text-xs-left">Tổng tiền</h3>
                                      <a class="text-xs-right totals_price_mobile" title="240.300₫">278.100₫</a>
                                   </div>
                                   <div class="checkout"><button class="btn-proceed-checkout-mobile" title="Thanh toán ngay" type="button" onclick="window.location.href='{{route('pay.index')}}'"><span>Thanh toán ngay</span></button></div>
                                   <button class="btn btn-proceed-continues-mobile" title="Tiếp tục mua hàng" type="button" onclick="window.location.href='/collections/all'">Tiếp tục mua hàng</button>
                                </div>
                             </div>
                          </form>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>
@endsection