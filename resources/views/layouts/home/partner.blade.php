<section class="awe-section-10">
    <div class="container section_blogs">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="new_title">
                    <h2><a href="tin-tuc" title="Đối tác">{{getLanguage('partner')}}</a></h2>
                    <p>{{getLanguage('our_typical_partners')}}</p>
                </div>
                <div class="evo-owl-blog evo-slick">
                    @foreach($partner as $item)
                    <div class="news-items">
                        <a href="{{$item->link == null ? 'javascript:;' : $item->link}}"
                    title="{{$item->name}}"
                            class="clearfix evo-item-blogs">
                            <div class="evo-article-image">
                                <img src="{{$item->image}}"
                                    data-src="{{$item->image}}"
                                    alt="{{$item->image}}"
                                    class="lazy img-responsive center-block" />
                            </div>
                            <h3 class="line-clamps">{{$item->name}}</h3>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>