<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8" />
    <title>@yield('title')</title>
    <meta name="description" content='@yield('title')'/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="robots"
          content="index, follow"/>

    <meta name="Googlebot"
          content="index, follow"/>
    <!-- Open Graph data -->
    <meta property="og:site_name" content="@yield('title')"/>
    <meta property="og:type" content="article"/>
    <meta property="og:locale" content="vi_VN"/>

    <meta property="og:title" content='@yield("title")'/>
    <meta property="og:description" content='@yield("description")'/>
    <meta property="og:url" content='@yield("url")'/>

    <meta property="og:image" itemprop="thumbnailUrl" content='@yield("images")'/>
    <meta property="og:image:width" content="600"/>
    <meta property="og:image:height" content="348"/>

    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:description" content='@yield("description")'/>
    <meta name="twitter:title" content='@yield("title")'/>
    <meta name="twitter:site" content="@ribetovn"/>


    <link rel="shortcut icon" href="{{$setting->favicon}}">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link href="{{asset('frontend/asset/css/bootstrap.scss.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('frontend/asset/css/plugin.scss.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('frontend/asset/css/base.scss.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('frontend/asset/css/evo-main.scss.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('frontend/asset/css/slick.scss.css')}}" rel="stylesheet" type="text/css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
        type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" type="text/javascript"></script>
    <script src="{{asset('frontend/asset/js/slick.js')}}" type="text/javascript"></script>
    <link href="{{asset('frontend/asset/css/evo-index.scss.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('frontend/asset/css/notify.css')}}" rel="stylesheet" type="text/css" />
    @yield('css')
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v5.0"></script>
</head>

<body class=" index">
    @include('layouts.header')
    
    <script src="{{asset('frontend/asset/js/option-selectors.js')}}" type="text/javascript"></script>
    <script src="{{asset('frontend/asset/js/api.jquery.js')}}" type="text/javascript"></script>
    
    <link href="http://bizweb.dktcdn.net/100/360/151/themes/727143/assets/picbox.scss.css" rel="stylesheet" type="text/css" />
    <script src="http://bizweb.dktcdn.net/100/360/151/themes/727143/assets/picbox.js" type="text/javascript"></script>
    <h1 class="hidden">{{$setting->company}}</h1>
    @yield('content')
    @yield('script_detail')
    @include('layouts.footer')
    <div class="backdrop__body-backdrop___1rvky"></div>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"
        type="text/javascript"></script>
    <div class="ajax-load">
        <span class="loading-icon">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                y="0px" width="24px" height="30px" viewBox="0 0 24 30" style="enable-background:new 0 0 50 50;"
                xml:space="preserve">
                <rect x="0" y="10" width="4" height="10" fill="#333" opacity="0.2">
                    <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0s" dur="0.6s"
                        repeatCount="indefinite" />
                    <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0s" dur="0.6s"
                        repeatCount="indefinite" />
                    <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0s" dur="0.6s"
                        repeatCount="indefinite" />
                </rect>
                <rect x="8" y="10" width="4" height="10" fill="#333" opacity="0.2">
                    <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0.15s" dur="0.6s"
                        repeatCount="indefinite" />
                    <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0.15s" dur="0.6s"
                        repeatCount="indefinite" />
                    <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0.15s" dur="0.6s"
                        repeatCount="indefinite" />
                </rect>
                <rect x="16" y="10" width="4" height="10" fill="#333" opacity="0.2">
                    <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0.3s" dur="0.6s"
                        repeatCount="indefinite" />
                    <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0.3s" dur="0.6s"
                        repeatCount="indefinite" />
                    <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0.3s" dur="0.6s"
                        repeatCount="indefinite" />
                </rect>
            </svg>
        </span>
    </div>
    <div class="loading awe-popup">
        <div class="overlay"></div>
        <div class="loader" title="2">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                y="0px" width="24px" height="30px" viewBox="0 0 24 30" style="enable-background:new 0 0 50 50;"
                xml:space="preserve">
                <rect x="0" y="10" width="4" height="10" fill="#333" opacity="0.2">
                    <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0s" dur="0.6s"
                        repeatCount="indefinite" />
                    <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0s" dur="0.6s"
                        repeatCount="indefinite" />
                    <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0s" dur="0.6s"
                        repeatCount="indefinite" />
                </rect>
                <rect x="8" y="10" width="4" height="10" fill="#333" opacity="0.2">
                    <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0.15s" dur="0.6s"
                        repeatCount="indefinite" />
                    <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0.15s" dur="0.6s"
                        repeatCount="indefinite" />
                    <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0.15s" dur="0.6s"
                        repeatCount="indefinite" />
                </rect>
                <rect x="16" y="10" width="4" height="10" fill="#333" opacity="0.2">
                    <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0.3s" dur="0.6s"
                        repeatCount="indefinite" />
                    <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0.3s" dur="0.6s"
                        repeatCount="indefinite" />
                    <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0.3s" dur="0.6s"
                        repeatCount="indefinite" />
                </rect>
            </svg>
        </div>
    </div>
    <div class="addcart-popup product-popup awe-popup">
        <div class="overlay no-background"></div>
        <div class="content">
            <div class="row row-noGutter">
                <div class="col-xl-6 col-xs-12">
                    <div class="btn btn-full btn-primary a-left popup-title"><i class="fa fa-check"></i>Thêm vào giỏ
                        hàng thành công
                    </div>
                    <a href="javascript:void(0)" class="close-window close-popup"><i class="fa fa-close"></i></a>
                    <div class="info clearfix">
                        <div class="product-image margin-top-5"><img alt="popup"
                                src="https://bizweb.dktcdn.net/100/360/151/themes/727143/assets/logo.png"
                                style="max-width:150px; height:auto" /></div>
                        <div class="product-info">
                            <p class="product-name"></p>
                            <p class="quantity color-main"><span>Số lượng: </span></p>
                            <p class="total-money color-main"><span>Tổng tiền: </span></p>
                        </div>
                        <div class="actions"><button class="btn  btn-primary  margin-top-5 btn-continue">Tiếp tục mua
                                hàng</button><button class="btn btn-gray margin-top-5"
                                onclick="window.location='/cart'">Kiểm tra giỏ hàng</button></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="error-popup awe-popup">
        <div class="overlay no-background"></div>
        <div class="popup-inner content">
            <div class="error-message"></div>
        </div>
    </div>
    <div id="popup-cart" class="modal fade" role="dialog">
        <div id="popup-cart-desktop" class="clearfix">
            <div class="title-popup-cart"><i class="ion ion-md-notifications-outline" aria-hidden="true"></i> Bạn đã
                thêm <span class="cart-popup-name"></span> vào giỏ hàng</div>
            <div class="title-quantity-popup"><a href="/cart" title="Xem giỏ hàng">Xem giỏ hàng</a></div>
            <div class="content-popup-cart clearfix">
                <div class="thead-popup">
                    <div style="width: 55%;" class="text-left">Sản phẩm</div>
                    <div style="width: 15%;" class="text-center">Đơn giá</div>
                    <div style="width: 15%;" class="text-center">Số lượng</div>
                    <div style="width: 15%;" class="text-center">Thành tiền</div>
                </div>
                <div id="cart-body">
                    <div class="tbody-popup" >

                    </div>
                    <div class="tfoot-popup">
                        <div class="tfoot-popup-1 clearfix">
                            <div class="pull-left popupcon"><a class="button btn-continue" title="Tiếp tục mua hàng"
                                    onclick="$('#popup-cart').modal('hide');"><span><span><i class="fa fa-caret-left"
                                                aria-hidden="true"></i> Tiếp tục mua hàng</span></span></a></div>
                            <div class="pull-right popup-total">
                            <p class="total_cart">Thành tiền: đ<span class="total-price"></span></p>
                            </div>
                        </div>
                        <div class="tfoot-popup-2 clearfix"><a class="button btn-proceed-checkout"
                                title="Thanh toán đơn hàng" href="/checkout"><span>Thanh toán đơn hàng</span></a></div>
                    </div>
                </div>
                
                
            </div>
            <a class="quickview-close close-window" href="javascript:;" onclick="$('#popup-cart').modal('hide');"
                title="Đóng"><i class="fa fa-times"></i></a>
        </div>
    </div>
    @if(Session::has('success'))
            <div class="lobibox-notify-wrapper top right">
                <div class="lobibox-notify lobibox-notify-success animated-fast fadeInDown without-icon notify-mini" style="width:368px">
                    <div class="lobibox-notify-icon-wrapper">
                        <div class="lobibox-notify-icon">
                            <div></div>
                        </div>
                    </div>
                    <div class="lobibox-notify-body">
                        <div class="lobibox-notify-msg" style="max-height:32px">{{ Session::get('success') }}</div>
                    </div>
                    <span class="lobibox-close" onclick="$('.lobibox-notify-wrapper').remove()">×</span>
                </div>
            </div>
            @php
                Session::forget('success');
            @endphp
        @endif
        @if(Session::has('error'))
            <div class="lobibox-notify-wrapper top right">
                <div class="lobibox-notify lobibox-notify-error animated-fast fadeInDown without-icon notify-mini" style="width:368px">
                    <div class="lobibox-notify-icon-wrapper">
                        <div class="lobibox-notify-icon">
                            <div></div>
                        </div>
                    </div>
                    <div class="lobibox-notify-body">
                        <div class="lobibox-notify-msg" style="max-height:32px">{{ Session::get('error') }}</div>
                    </div>
                    <span class="lobibox-close" onclick="$('.lobibox-notify-wrapper').remove()">×</span>
                </div>
            </div>
            @php
                Session::forget('error');
            @endphp
        @endif
    <div id="myModal" class="modal fade" role="dialog"></div>
    <script src="https://cdn.jsdelivr.net/npm/intersection-observer@0.5.1/intersection-observer.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
    <script src="{{asset('frontend/asset/js/cs.script.js')}}" type="text/javascript"></script>
    <script src="{{asset('frontend/asset/js/main.js')}}" type="text/javascript"></script>
    <script src="{{asset('frontend/asset/js/evo-index-js.js')}}" type="text/javascript"></script>
    <script src="{{asset('frontend/asset/js/cart.js')}}" type="text/javascript"></script>
    @yield('script')
    <script>
            $('.btn-open-nvmb').click(function(){
                $('body').append("<div class='ui-app-frame__backdrop ui-app-frame__backdrop--is-visible' onclick='closeMN()'></div>");
                $('#AppFrameAside').toggleClass('ui-app-frame__aside--is-open');
            })
            function closeMN() {
                $('#AppFrameAside').removeClass('ui-app-frame__aside--is-open');
                $('.ui-app-frame__backdrop').remove()
            }
            if ($('.lobibox-notify-wrapper').length > 0) {
                $('.lobibox-notify-wrapper').delay(3000).fadeOut(1000);
            }
            $(document).ready(function() { 
                $("#type_prod").select2(); 
                $("#vendor_prod").select2(); 
                $("#cate_prod").select2();
            });
        </script>
</body>

</html>