<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
});
Route::get('/admin', function () {
    return view('app');
});
Route::get('/','HomeController@home')->name('home');
Route::group(['namespace'=>'Client'], function(){
    Route::group(['prefix'=>'san-pham'], function(){
        Route::get('/','ProductController@list')->name('product.list');
        Route::get('filter-product','ProductController@filter_product')->name('product.filter-product');
        Route::get('/{slug}','ProductController@detail_product')->name('product.detail');
    });
    Route::group(['prefix'=>'tin-tuc'], function(){
        Route::get('/','BlogController@list')->name('blog.list');
        Route::get('/{slug}','BlogController@detail')->name('blog.detail');
    });
    Route::group(['prefix'=>'cart'], function(){
        Route::get('/','CartController@cart_list')->name('cart.list');
        Route::get('create/{id}','CartController@create')->name('cart.add');
        Route::get('buy-now/{id}','CartController@buld_now')->name('cart.now');
        Route::get('update/{rowId}/{qty}','CartController@update')->name('cart.update');
        Route::get('delete/{rowId}','CartController@delete')->name('cart.delete');
    });
    Route::group(['prefix'=>'thanh-toan'], function(){
        Route::get('/','PayController@index')->name('pay.index');
        Route::post('/','PayController@postPay')->name('pay.post');
    });
    Route::get('page/{slug}','PageContentController@detail')->name('pagecontent.detail');
    Route::get('lien-he','ContactController@index')->name('contact.index');
    Route::get('/dang-nhap','AuthController@login')->name('login.index');
    Route::post('/dang-nhap','AuthController@postLogin')->name('login.post');
    Route::get('/dang-ky','AuthController@register')->name('register.index');
    Route::post('/dang-ky','AuthController@postRegister')->name('register.post');
    Route::get('/dang-xuat','AuthController@logout')->name('logout.index');
    Route::post('reset-password', 'ResetPasswordController@sendMail')->name('resetPassword.index');
    Route::post('get-reset-password/{token}', 'ResetPasswordController@reset')->name('resetPassword.reset');
    Route::get('get-reset-password/{token}', 'ResetPasswordController@getReset')->name('resetPassword.getReset');
});


Route::post('/languages', 'LanguageController@index')->name('languages');
Route::get('/pusher', function() {
    event(new App\Events\HelloPusherEvent('Hi there Pusher!'));
    return "Event has been sent!";
});