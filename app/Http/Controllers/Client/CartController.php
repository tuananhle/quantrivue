<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\product\Product;
use Cart;
use Session;

class CartController extends Controller
{
    public function create($id)
    {
		$language_current = Session::get('locale');
    	$shopping_pro = Product::find($id);
    	$arrImg = json_decode($shopping_pro->images);
		Cart::add([
				'id' => $id,
			 	'name' => $shopping_pro->name,
				'qty' => 1, 
				'price' => $shopping_pro->price, 
				'weight' => 0,
				'options' => ['discount' => $shopping_pro->discount ? $shopping_pro->discount : 0,'img'=>$arrImg[0],'slug'=>$shopping_pro->slug,'code'=> $shopping_pro->code]
		  ]);
		$data['cart'] = Cart::content();
		$data['total'] = Cart::total();
		$data['cartcount'] = Cart::count();
    	return response()->json($data);
	}
	public function buld_now($id)
    {
		$language_current = Session::get('locale');
    	$shopping_pro = Product::find($id);
    	$arrImg = json_decode($shopping_pro->images);
		Cart::add([
				'id' => $id,
			 	'name' => $shopping_pro->name,
				'qty' => 1, 
				'price' => $shopping_pro->price, 
				'weight' => 0,
				'options' => ['discount' => $shopping_pro->discount ? $shopping_pro->discount : 0,'img'=>$arrImg[0],'slug'=>$shopping_pro->slug,'code'=> $shopping_pro->code]
		  ]);
		$data['cart'] = Cart::content();
		$data['total'] = Cart::total();
		$data['cartcount'] = Cart::count();
    	return view('pay.index',$data);
	}
	public function update(Request $request,$rowId,$qty)
	{

		Cart::update($rowId, ['qty' => $qty]); 
		$data['cart'] = Cart::content();
		$data['total'] = Cart::total();
		$data['cartcount'] = Cart::count();
    	return response()->json($data);
    	
	}
	public function cart_list(Request $request)
	{
		$data['cart'] = Cart::content();
		$data['total'] = Cart::total();
		$data['cartcount'] = Cart::count();
		return view('cart.list',$data);
	}
	public function delete(Request $request,$rowId)
	{
		Cart::remove($rowId);
		$data['cart'] = Cart::content();
		$data['total'] = Cart::total();
		$data['cartcount'] = Cart::count();
    	return response()->json($data);
	}
}
